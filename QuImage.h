#pragma once
#include "IwGL.h"
#include <string>
#include <set>
#include "png.h"
#include "QuMath.h"
#include "s3eFile.h"
#include "s3eMemory.h"
#include <iostream>
#include "QuClutils.h"
#include "QuUtils.h"

class PngLoader
{
	bool bDone;
	bool bReady;

	//s3e file stuff
	s3eFile *fileHandle;

	//png info
	int mNumberPasses;
	png_structp png_ptr;
	png_infop info_ptr;

	//additional png info
	png_uint_32 mW;
	png_uint_32 mH;
	int potW,potH;
	int mBitDepth;
	int mColorType;
	int bytesPerPixel;
	png_uint_32 bytesPerRow;

	//update cycle info
	int linesRead;

	//image data
	void * imageData;
	png_bytepp rowPointerData;

public:

	static void readDataFromFileHandle(png_structp png_ptr, png_bytep outBytes, png_size_t byteCountToRead);

	PngLoader()
	{
		imageData = NULL;
		rowPointerData = NULL;
		linesRead = 0;
		bDone = false;
		bReady = false;
	}
	~PngLoader()
	{
		abort();
		deleteImageData();
	}
	void abort();
	void deleteImageData();
	bool setFile(std::string filename);
	bool update(int rowsToRead);
	int getWidth() { return mW; }
	int getHeight() { return mH; }
	int getPotWidth() { return potW; }
	int getPotHeight() { return potH; }
	float getWidthRatio() const { return mW/(float)potW; }
	float getHeightRatio() const { return mH/(float)potH; }
	int getImageTexelSize()	{ return potW*potH*bytesPerPixel; }
	unsigned char * getImageData(){ return (unsigned char *)imageData; }
	bool isPngDoneLoading(){ return bDone; }
	GLenum getFormat();
	bool isPngReadyToLoad(){ return bReady; }
private:

};

class QuBaseImage
{
protected:
	bool isLoaded; 
	GLuint mId;
	//true image w/h
	int mWidth,mHeight;
	//texture dimensions
	int mPotW, mPotH;
	//used by derive classes that want to destroy their own overhead and return a QuBaseImage
	QuStupidPointer<QuBaseImage> reincarnate();
	static int uploadTexture(GLenum aFormat, int aWidth, int aHeight,GLvoid * aData)
	{
		GLuint id = -1;
		glPixelStorei(GL_UNPACK_ALIGNMENT,1);
		glGenTextures(1,&id);
		glBindTexture(GL_TEXTURE_2D,id);
		glTexParameterx(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterx(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, aFormat, aWidth, aHeight, 0, aFormat, GL_UNSIGNED_BYTE, aData);
		return id;
	}
public:
	QuBaseImage():isLoaded(false),mId(-1){}
	int getWidth() const { return mWidth; }
	int getHeight() const { return mHeight; }
	int getPotW() const { return mPotW; }
	int getPotH() const { return mPotH; }
	float getWidthToHeightRatio() const { quAssert(mHeight > 0); return mWidth/(float)mHeight; }
	float getHeightToWidthRatio() const { quAssert(mWidth > 0); return mHeight/(float)mWidth; }
	bool isImageLoaded() const { return isLoaded; }
	void setIdAndLoad(int _id);
	virtual bool bind();
	void unbind();
	//SHADY, caller responsible for deleting the memory
	float * getUVData();

	//TODO figure out why thi sdestroy is not calling the derived subclasses destroy
	virtual ~QuBaseImage(){destroy();}
	virtual void destroy();
private:
	QuBaseImage(QuBaseImage & o){ quAssert(false); }
};

//this image has pixel modifying functions, yay
//TODO should give this a no alpha mode
class QuDynamicImage : public QuBaseImage
{
	bool mIsTextureSet;
	QuStupidPointer<unsigned char> mImageData;
public:
	QuDynamicImage(){}
	QuDynamicImage(int _w, int _h){initialize(_w,_h);}
	//TODO qubaseimage should be calling this :(, not a huge deal
	~QuDynamicImage()
	{destroy();}
	void destroy() { mImageData.setNull(); QuBaseImage::destroy(); }
	virtual bool bind(){if(!mIsTextureSet) setTexture(); return QuBaseImage::bind();}
	void initialize(int _w, int _h);
	//QuStupidPointer<unsigned char> getImageDataToModify() { mIsTextureSet = false; return mImageData; }
	bool safeSetPixel(int _x, int _y, unsigned char _r, unsigned char _g, unsigned char _b, unsigned char _a);
	void setPixel(int _x, int _y, unsigned char _r, unsigned char _g, unsigned char _b, unsigned char _a);
	bool safeGetPixel(int _x, int _y, unsigned char & _r, unsigned char & _g, unsigned char & _b, unsigned char & _a);
	void getPixel(int _x, int _y, unsigned char & _r, unsigned char & _g, unsigned char & _b, unsigned char & _a);
	void setTexture();
};

class QuPngImage : public QuBaseImage
{
	QuStupidPointer<PngLoader> loader;	
	std::string mFilename;
public:
	QuPngImage():loader(NULL){}
	//TODO qubaseimage should be calling this :(, not a huge deal
	~QuPngImage(){destroy();}
	void destroy();
	bool reloadImage();
	//returns true if image is done loading
	bool pumpImage(int rows);
	bool finishLoadingImage();

	bool loadImage(std::string filename);
	bool loadEntireImage(std::string filename);
	

private:
	bool setTexture();
};



