#pragma once

#include "QuUtils.h"
#include "QuConstants.h"

class QuFunctionTesselator
{
public:
	//TODO intertwine the normal array
	static float * basicTesselator(float *(*f)(float),float *(*g)(float), int steps, std::vector<float> poi)
	{
		//create a list of time steps
		std::vector<float> tv(steps+1);
		for(int i = 0; i < steps; i++)
			tv[i] = i/(float)(steps-1);
		tv[steps] = 2;	//dummy point so we do not need to make special cases in loops to follow

		//note this algorithm replaces points with poi rather than inserts them hence it is possible for one poi to replace another if steps is not large enough
		for(int i = 0; i < poi.size(); i++)
		{
			for(int j = 0; j < steps; j++)
			{
				if(poi[i] >= tv[j] && poi[i] < tv[j+1])
				{
					//see which one is closer 
					if(poi[i] - tv[j] <= tv[j+1] - poi[i])
						tv[j] = poi[i];
					else
						tv[j+1] = poi[i];
				}
			}
		}

		//new we begin constructing our triangle strip point list
		float * r = new float[steps*2*3];
		for(int i = 0; i < steps; i++)
		{
			float * fVal = f(tv[i]);
			float * gVal = g(tv[i]);
			memcpy(r+2*3*i,fVal,sizeof(float)*3);
			memcpy(r+2*3*i+3,gVal,sizeof(float)*3);
			delete [] fVal;
			delete [] gVal;
		}
		return r;
	}

	//draws a square centered at 0,0,0 with width 1
	//note this function will leak if return result is not cleaned up.
	template<int num, int denom>
	static float * square(float t)
	{
		float halfWidth = num/(float)denom/(float)2;
		float * r = new float[3];
		r[0] = 0;
		float * lerpC;
		int sign;
		float offset = int(t*4)*0.25f;
		if(t < 0.25)
		{
			r[1] = -halfWidth;
			lerpC = r+2;
			sign = 1;
		}
		else if(t < 0.5)
		{
			r[2] = halfWidth;
			lerpC = r+1;
			sign = 1;
		}
		else if(t < 0.75)
		{
			r[1] = halfWidth;
			lerpC = r+2;
			sign = -1;
		}
		else
		{
			r[2] = -halfWidth;
			lerpC = r+1;
			sign = -1;
		}

		//special case
		if(offset == 1)
		{
			r[1] = -halfWidth;
			return r;
		}

		*lerpC = sign*(t-offset)*4 - sign*halfWidth;
		return r;
	}
	
	static std::vector<float> squarePoi()
	{
		std::vector<float> r(4);
		r[0] = 0;
		r[1] = 0.25f;
		r[2] = 0.5f;
		r[3] = 0.75f;
		return r;
	}
	
	template<int rNum, int rDenom, int zNum, int zDenom>
	static float* circle(float t)
	{
		float zOffset = zNum/(float)zDenom;
		float radius = rNum/(float)rDenom;
		float * r = new float[3];
		r[0] = zOffset;
		r[2] = radius*cos(t*2*PI-PI*3/4);
		r[1] = radius*sin(t*2*PI-PI*3/4);
		return r;
	}

	static std::vector<float> combinePoi(std::vector<float> v, std::vector<float> u)
	{
		std::vector<float> r;
		for(int i = 0; i < v.size(); i++)
		{
			r[i] = v[i];
		}
		for(int i = 0; i < u.size(); i++)
		{
			r[i+v.size()] = u[i];
		}
		return r;
	}

};