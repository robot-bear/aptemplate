#pragma once
#include "QuConstants.h"
#include "QuUtils.h"
#include "QuCMath.h"
#include "QuCamera.h"
#include "QuDrawer.h"
#include "IwGL.h"
#include <string>
#include <list>
#include <vector>


class QuContext
{
public:
	virtual void enable() = 0;
	//note disable does not guarantee to reset program state back to what it was before enable
	virtual void disable(){};
};



class QuGLStateSpecialContext : QuContext
{
	bool mDepthMaskEnable,mDepthMaskDisable;
	bool mUseBlendFunc;
	GLenum mBlendFuncSrc, mBlendFuncDst;
public:
	QuGLStateSpecialContext()
	{
		mDepthMaskEnable = mDepthMaskDisable = false;
		mUseBlendFunc = false;
	}
	void setBlendFunc(bool aUseBlendFunc, GLenum aSrc, GLenum aDst)
	{
		mUseBlendFunc = aUseBlendFunc;
		mBlendFuncSrc = aSrc;
		mBlendFuncDst = aDst;
	}
	void setDepthMask(bool aEnable = true, bool aDisable = false)
	{
		mDepthMaskEnable = aEnable;
		mDepthMaskDisable = aDisable;
	}
	void enable()
	{
		if(mDepthMaskEnable)
			glDepthMask(true);
	}
	void disable()
	{
		if(mDepthMaskDisable)
			glDepthMask(false);
	}
};


//TODO this should do a map from GLenum to your bitshift enable disable thingy
/*class QuGLStateContext : QuContext
{

	std::vector<GLenum> mEnableList;
	std::vector<GLenum> mDisableList;
	
public:
	void addParameter(GLenum aParam, bool enable = true, bool disable = true)
	{
		if(enable)
			mEnableList.push_back(aParam);
		if(disable)
			mDisableList.push_back(aParam);
	}
	void enable()
	{
		for(std::vector<GLenum>::iterator it = mEnableList.begin(); it != mEnableList.end(); it++)
			glEnable(*it);
	}
	void disable()
	{
		for(std::vector<GLenum>::iterator it = mDisableList.begin(); it != mDisableList.end(); it++)
			glDisable(*it);
	}
	
};*/


//TODO test me
class QuGLStateContext : QuContext
{
	std::map<GLenum,char> mStateVars;
	enum QuGLStateBit
	{
		QUGLSTATE_ENABLE_BIT = 1,			//we modify this bit in enable(), true -> do it
		QUGLSTATE_ENABLE_ENABLE_BIT = 2,	//do we call glEnable or glDisable(), true -> enable
		QUGLSTATE_DISABLE_BIT = 4,			//do we modify this bit in disable(), true -> do it
		QUGLSTATE_DISABLE_ENABLE_BIT = 2	//enable or disable, true -> enable (tricky)

	};
public:
	//if disable is set to true it will 
	void addParameter(GLenum aParam, bool doEnable, bool onEnableEnable, bool doDisable, bool onDisableEnable)
	{
		char toSet = 0;
		if(doEnable)
			toSet |= QUGLSTATE_ENABLE_BIT;
		if(onEnableEnable)
			toSet |= QUGLSTATE_ENABLE_ENABLE_BIT;
		if(doDisable)
			toSet |= QUGLSTATE_DISABLE_BIT;
		if(onDisableEnable)
			toSet |= QUGLSTATE_DISABLE_ENABLE_BIT;
		mStateVars[aParam] = toSet;
	}
	void enable()
	{
		for(std::map<GLenum,char>::iterator it = mStateVars.begin(); it != mStateVars.end(); it++)
		{
			if(it->second & QUGLSTATE_ENABLE_BIT)
			{
				if(it->second & QUGLSTATE_ENABLE_ENABLE_BIT)
					glEnable(it->first);
				else
					glDisable(it->first);
			}
		}
	}
	void disable()
	{
		for(std::map<GLenum,char>::iterator it = mStateVars.begin(); it != mStateVars.end(); it++)
		{
			if(it->second & QUGLSTATE_DISABLE_BIT)
			{
				if(it->second & QUGLSTATE_DISABLE_ENABLE_BIT)
					glEnable(it->first);
				else
					glDisable(it->first);
			}
		}
	}
	
};

class QuGLTotalStateContext : QuContext
{
	QuGLStateSpecialContext mSpecial;
	QuGLStateContext mState;
public:
	void enable()
	{
		mSpecial.enable();
		mState.enable();
	}
	void disable()
	{
		mSpecial.disable();
		mState.disable();
	}
	enum QuGLStateContextStaticContextIds
	{
		QUGLSTATECONTEXT_GUI_DISABLE,
		QUGLSTATECONTEXT_GUI_NO_DISABLE,
	};
	static QuStupidPointer<QuGLTotalStateContext> getStaticContext(QuGLStateContextStaticContextIds id)
	{
		QuStupidPointer<QuGLTotalStateContext> r(new QuGLTotalStateContext());
		switch(id)
		{
		case QUGLSTATECONTEXT_GUI_NO_DISABLE:
			glEnable(GL_BLEND);
			//glBlendFunc(GL_DST_ALPHA, GL_SRC_ALPHA);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			glDepthMask(false);
			glDisable(GL_DEPTH_TEST);
			glDisable(GL_LIGHTING);
			r->mState.addParameter(GL_DEPTH_TEST,true,true,false,false);
			r->mState.addParameter(GL_LIGHTING,true,false,false,true);
			r->mSpecial.setDepthMask(true,false);
			r->mSpecial.setBlendFunc(true,GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA);
		default:
			return r;
		}
	}

};

//legacy
//TODO should add option to set whether to rotate or not
// add a getCameraRotation fcn
class QuGuiRenderingContext : QuContext
{
	QuBasicCamera mCamera;
	bool mScaleToScreen;
	GDeviceRotation mRot;
public:
	QuGuiRenderingContext():mCamera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT),mScaleToScreen(false),mRot(G_DR_FOLLOW){}
	void setScaleToScreen(bool aScale)
	{
		mScaleToScreen = aScale;
	}
	void setRotationType(GDeviceRotation aRot)
	{
		mRot = aRot;
		if(mRot != G_DR_FOLLOW)
			mCamera.setRotation(aRot,0);
	}
	GDeviceRotation getRotationType()
	{
		return mRot;
	}
	void enable()
	{
		if(mRot == G_DR_FOLLOW)
			mCamera.setRotation(G_DEVICE_ROTATION,0);
		mCamera.setScene();
		if(mScaleToScreen)
			glScalef(mCamera.getRotatedWidth(),mCamera.getRotatedHeight(),1);

		glEnable(GL_BLEND);
		//glBlendFunc(GL_DST_ALPHA, GL_SRC_ALPHA);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(false);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);
	}
	void disable()
	{
		glEnable(GL_LIGHTING);
		glEnable(GL_DEPTH_TEST);
		glDepthMask(true);
		glDisable(GL_BLEND);
	}
};

//legacy
class QuTintableGuiImage
{
	QuGuiRenderingContext mContext;
	QuColorableDrawObject<int> mDrawObject;
	QuTimer mTimer;
	QuArrayInterpolator<float,4> mColor;
	bool mIsUseColor;
public:
	QuTintableGuiImage():mTimer(0,30),mIsUseColor(true)
	{
		float c[4] = {0.6,0.6,0.8,1};
		mColor.setBaseValue(c);
		float d[4] = {0.8,0.6,0.6,1};
		mColor.setTargetValue(d);
	}
	~QuTintableGuiImage()
	{
	}
	void useColor(bool aUseColor)
	{
		mIsUseColor = aUseColor;
		if(!mIsUseColor)
			mDrawObject.clearColors();
	}
	void update()
	{
		if(mIsUseColor)
		{
			mTimer.update();
			float * fourc = cpcpcpVector<float>(mColor.interpolate(mTimer.getLinear()).d,4,4);
			mDrawObject.addColor(1,fourc,4);
			mDrawObject.setKey(1);
		}
	}
	
	void unloadImage()
	{
		mDrawObject.unloadImage();
	}
		 
	//TODO
	bool loadImage(QuStupidPointer<QuBaseImage> img)
	{ 
		mDrawObject.setCount(4);
		mDrawObject.loadImage(img);
		float * rectCoords = new float[4*3];
		memcpy(rectCoords,GUI_RECT_COORDS,sizeof(float) * 4 * 3);
		mDrawObject.loadVertices(rectCoords);
		return true;
	}
	bool loadImage(std::string aFilename, float * aTexData = NULL)
	{
		std::cout << "loading QuTintableImage by filename is deprecated" << std::endl;
		mDrawObject.setCount(4);
		if(aTexData == NULL)
		{
			aTexData = new float[4*2];
			memcpy(aTexData,GUI_RECT_UV,sizeof(float)*4*2);
		}
		if(!mDrawObject.loadImage(aFilename, aTexData))
			return false;
		//otherwise, if image loaded succesfully, we generate the rest of the info
		float * rectCoords = new float[4*3];
		memcpy(rectCoords,GUI_RECT_COORDS,sizeof(float) * 4 * 3);
		mDrawObject.loadVertices(rectCoords);
		return true;
	}
	void draw()
	{
		glPushMatrix();
		mContext.setScaleToScreen(true);
		mContext.enable();
		mDrawObject.autoDraw();
		mContext.disable();
		glPopMatrix();
	}
	void draw(float x, float y, float rot, float s = 1)
	{
		glPushMatrix();
		mContext.setScaleToScreen(false);
		mContext.enable();
		glScalef(1, 1, 1);
		//TODO should use mContext.getCameraRotation instead of G_DEVICE_ROTATION
		QuVector2<int> dim = G_GAME_GLOBAL.getRotatedScreenDimensions(mContext.getRotationType());
  		glTranslatef(-dim.x/2, dim.y/2, 0);
		glTranslatef(x,-y,0);
		glScalef(s,s,s);
		glRotatef(rot,0,0,1);
		mDrawObject.autoDraw();
		mContext.disable();
		glPopMatrix();
		
	}
	void setColor(float aColor[4])
	{
		mColor.setBaseValue(aColor);
		mColor.setTargetValue(aColor);
		update();
	}
	void setBaseColor(float aColor[4])
	{
		mColor.setBaseValue(aColor);
	}
	void setTargetColor(float aColor[4])
	{
		setBaseColor(mColor.interpolate(mTimer.getLinear()).d);
		mColor.setTargetValue(aColor);
		mTimer.reset();
	}
private:
};


//legacy
struct QuConstantForceParticle
{
	float x,y,rot,s;
	float vx,vy,vr,vs;
	float ax,ay;
	QuTimer mTimer;
	QuArrayInterpolator<float,4> mColor;
	bool isUseColor;
	QuConstantForceParticle(unsigned profile = 0)
	{
		switch(profile)
		{
		case 1: //awesome job
			isUseColor = true;
			x = y = ax = ay = vx = vy = rot = vr =0;
			s = 5;
			vs = 30;
			mTimer.setTargetAndReset(10);
			mColor.setBaseValue(FULL_OPAQUE);
			mColor.setTargetValue(FULL_TRANSPARENT);
			break;
		case 2:	//really awesome job
			isUseColor = true;
			mTimer.setTargetAndReset(5);
			mColor.setBaseValue(DARK_RED_OPAQUE);
			mColor.setTargetValue(FULL_TRANSPARENT);
			x = y = 0;
			vx = quRandfSym()*30;
			vy = quRandfSym()*30;
			ax = 0;
			ay = 0;
			vr = quRandfSym()*3;
			s = 30;
			vs = -2;	//set to 20, pretty cool
			break;
		case 3: //fail
			isUseColor = true;
			mTimer.setTargetAndReset(5);
			mColor.setBaseValue(DARK_RED_OPAQUE);
			mColor.setTargetValue(FULL_TRANSPARENT);
			x = y = 0;
			vx = quRandfSym()*30;
			vy = quRandfSym()*30;
			ax = 0;
			ay = 0;
			vr = quRandfSym()*3;
			s = 30;
			vs = -2;	//set to 20, pretty cool
			break;
		default:
			isUseColor = true;
			mTimer.setTargetAndReset(35);
			mColor.setBaseValue(FULL_BLUE_OPAQUE);
			mColor.setTargetValue(FULL_TRANSPARENT);
			x = y = 0;
			vx = quRandfSym()*15;
			vy = quRandfSym()*15;
			ax = 0;
			ay = 0;
			vr = quRandfSym()*3;
			s = 30;
			vs = -2;	//set to 20, pretty cool
			break;
		}
	}
	void setPosition(int argx, int argy)
	{
		x = argx; y = argy;
	}
	ARRAYN<float,4> getColor()
	{
		if(!isUseColor)
			return ARRAYN<float,4>(FULL_OPAQUE);
		return ARRAYN<float,4>(mColor.interpolate(mTimer.getLinear()).d);
	}
	void update()
	{
		mTimer.update();
		x += vx;
		y += vy;
		rot += vr;
		s += vs;

		vx += ax;
		vy += ay;
	}
	bool isInBox(QuBox2<int> rect)
	{
		return rect.doesContainPoint(QuVector2<int>(x,y));
	}
};

//legacy
class QuFireworks
{
	QuTintableGuiImage mImg;
	std::list<QuConstantForceParticle> mParts;
public:
	QuFireworks()
	{
		mImg.loadImage(std::string("images/fireworks.png"));
	}
	~QuFireworks()
	{
	}
	void stroke(int x, int y, float v)
	{
		if(v > 1)
		{
			fireworks(x,y,(int)v);
		}
	}
	void fireworks(int x, int y, int quantity)
	{
		for(int i = 0; i < quantity; i++)
		{
			
			mParts.push_back(QuConstantForceParticle(1));
			mParts.back().setPosition(x,y);

			float r = HALF_PI;
			mParts.back().ax = 5*cos(r);
			mParts.back().ay = 5*sin(r);
		}
	}
	void draw()
	{
		for(std::list<QuConstantForceParticle>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			if(i->isUseColor)
				mImg.setColor(i->getColor().d);
			//std::cout << i->x << " " << i->y << " " << i->s << std::endl;
			mImg.draw(i->x,i->y,i->rot,i->s);
		}
	}
	void update()
	{
		//delete particles 
		std::list<std::list<QuConstantForceParticle>::iterator> removal;
		for(std::list<QuConstantForceParticle>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			i->update();
			//TODO handle particle destruction
			int exp = 20;
			if(i->mTimer.isExpired())
			{
				removal.push_back(i);
			}
			else if(!i->isInBox(QuBox2<int>(0-exp,0-exp,G_SCREEN_WIDTH + 2*exp,G_SCREEN_HEIGHT + 2*exp)))
			{
				//removal.push_back(i);
			}
			
		}
		for(std::list<std::list<QuConstantForceParticle>::iterator>::iterator i = removal.begin(); i != removal.end(); i++)
			mParts.erase(*i);
	}
private:
};

//legacy
class QuTouchEffect
{
	QuTintableGuiImage mImg;
	std::list<QuConstantForceParticle> mParts;
public:
	QuTouchEffect()
	{
		mImg.loadImage(std::string("images/transdot-01.png"));
	}
	~QuTouchEffect()
	{
	}
	void touch(int x, int y)
	{
		for(int i = 0; i < 1; i++)
		{
			mParts.push_back(QuConstantForceParticle(1));
			mParts.back().setPosition(x,y);
		}
	}
	void draw()
	{
		for(std::list<QuConstantForceParticle>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			if(i->isUseColor)
				mImg.setColor(i->getColor().d);
			mImg.draw(i->x,i->y,i->rot,i->s);
		}
	}
	void update()
	{
		std::list<std::list<QuConstantForceParticle>::iterator> removal;
		for(std::list<QuConstantForceParticle>::iterator i = mParts.begin(); i != mParts.end(); i++)
		{
			i->update();
			//TODO handle particle destruction
			int exp = 20;
			if(i->mTimer.isExpired())
				removal.push_back(i);
			else if(!i->isInBox(QuBox2<int>(0-exp,0-exp,G_SCREEN_WIDTH + 2*exp,G_SCREEN_HEIGHT + 2*exp)))
				removal.push_back(i);
			
		}
		for(std::list<std::list<QuConstantForceParticle>::iterator>::iterator i = removal.begin(); i != removal.end(); i++)
			mParts.erase(*i);
	}
private:
};


inline void drawRectangle(QuBox2<int> r, float color [4])
{
	QuGuiRenderingContext render;
	render.setScaleToScreen(false);
	render.enable();
	//invert the y
	glScalef(1,-1,1);
	glTranslatef(-G_SCREEN_WIDTH/2,-G_SCREEN_HEIGHT/2,0);
	//make the points
	float verts [4*3] = 
	{
		r.x, r.y, 0,
		r.x + r.w, r.y, 0,
		r.x, r.y + r.h, 0,
		r.x + r.w, r.y + r.h, 0
	};
	float colors [4 * 4] =
	{
		color[0],color[1],color[2],color[3],
		color[0],color[1],color[2],color[3],
		color[0],color[1],color[2],color[3],
		color[0],color[1],color[2],color[3]
	};

	glEnableClientState(GL_VERTEX_ARRAY);
	glEnableClientState(GL_COLOR_ARRAY);
	glVertexPointer(3,GL_FLOAT,0,verts);
	glColorPointer(4,GL_FLOAT,0,colors);
	//glDrawArrays(GL_LINE_STRIP,0,4);
	glDrawArrays(GL_TRIANGLE_STRIP,0,4);
	glDisableClientState(GL_VERTEX_ARRAY);
	glDisableClientState(GL_COLOR_ARRAY);
}

//TODO reimplimint this as a specialization of QuButtonGroup
class QuButtonGroup
{
	std::vector<QuBox2<int> > mRects;
public:
	void addButton(QuBox2<int> aR)
	{
		mRects.push_back(aR);

	}
	//returns -1 if nothing was pressed, otherwise the index of the vector
	int isButtonPressed(int x, int y)
	{
		for(int i = 0; i < mRects.size(); i++)
		{
			if(mRects[i].doesContainPoint(QuVector2<int>(x,y)))
				return i;
		}
		return -1;
	}
	void drawSelection()
	{
		QuGuiRenderingContext render;
		render.setScaleToScreen(false);
		render.enable();
		//invert the y
		glScalef(1,-1,1);
		glTranslatef(-G_SCREEN_WIDTH/2,-G_SCREEN_HEIGHT/2,0);
		for(int i = 0; i < mRects.size(); i++)
		{
			QuBox2<int> r =  mRects[i];
			//make the points
			float verts [4*3] = 
			{
				r.x, r.y, 0,
				r.x + r.w, r.y, 0,
				r.x, r.y + r.h, 0,
				r.x + r.w, r.y + r.h, 0
			};
			/*
			float colors [4 * 4] =
			{
				1,0,0,1,1,0,0,1,1,0,0,1,1,0,0,1
			};*/

			float colors [4 * 4] =
			{
				0,0,0,1, 0,0,0,1, 0,0,0,1, 0,0,0,1,
			};

			glEnableClientState(GL_VERTEX_ARRAY);
			glEnableClientState(GL_COLOR_ARRAY);
			glVertexPointer(3,GL_FLOAT,0,verts);
			glColorPointer(4,GL_FLOAT,0,colors);
			glDrawArrays(GL_LINE_STRIP,0,4);
			//glDrawArrays(GL_TRIANGLE_STRIP,0,4);
			glDisableClientState(GL_VERTEX_ARRAY);
			glDisableClientState(GL_COLOR_ARRAY);
		}
	}
};
class QuMenuItem : public QuButtonGroup
{
	std::string mFn;
	bool isSet;
	bool lazyLoading;
	QuTintableGuiImage mImg;

public:
	QuMenuItem(bool ll = false)
	{
		lazyLoading = ll;
		isSet = false;
		mImg.useColor(false);
	}
	void loadImage()
	{
		if(isSet && lazyLoading)
			mImg.loadImage(mFn);
	}
	void setImage(std::string filename)
	{
		mFn = filename;
		isSet = true;
		if(!lazyLoading)
			mImg.loadImage(mFn);
	}
	void unloadImage()
	{
		mImg.unloadImage();
	}
	void draw()
	{
		mImg.draw();
	}
	void update()
	{
		//no need to update the image since it does nto change color
	}
};

//TODO redesign these
//TODO has undo buttons and music change/mute options if you ever get to that.
//NOTE drawing of the menu items is actually handled by QuOverlayer for convenience (since I wrote that code already to take care of fading and stuff)
class QuGameMenu
{
	QuMenuItem undo, music, mute;
public:
	QuGameMenu()
	{
		undo.addButton(QuBox2<int>(0,0,G_SCREEN_WIDTH/4,G_SCREEN_HEIGHT/4));
		undo.addButton(QuBox2<int>(0-G_SCREEN_WIDTH/4,0-G_SCREEN_HEIGHT/4,G_SCREEN_WIDTH/4,G_SCREEN_HEIGHT/4));
	}
	~QuGameMenu()
	{
	}
	void update()
	{
		//no need to update the buttons
	}
	bool pmusic(int x, int y)
	{
		return false;
	}
	bool pmute(int x, int y)
	{
		return false;
	}
	bool pundo(int x, int y)
	{
		return undo.isButtonPressed(x,y);
	}
};


//TODO redesign these
class QuTransition
{
public:
	virtual void update(){}
	virtual void draw(){}
	virtual bool isFinished() = 0;
};

class QuSlidingTransition : QuTransition
{
	QuTintableGuiImage mImg;
	QuTimer mSlideTimer;
	QuTimer mPauseTimer;
	QuArrayInterpolator<float,4> mColor;
public:
	QuSlidingTransition():mSlideTimer(0,10),mPauseTimer(0,8)
	{
		mColor.setBaseValue(THREE_QUARTER_OPAQUE);
		mColor.setTargetValue(FULL_TRANSPARENT);
		mPauseTimer.expire();
	}
	virtual void update()
	{
		if(mPauseTimer.isExpired())
		{
			mSlideTimer--;
		}
		else if(mSlideTimer.isExpired())
		{
			mPauseTimer++;
		}
		else 
		{
			mSlideTimer++;
		}
	}
	void loadImage(std::string filename)
	{
		mImg.loadImage(filename);
	}
	void reset()
	{
		mPauseTimer.reset();
		mSlideTimer.reset();
	}
	virtual void draw()
	{
		//TODO calculate position and stuff
		float y;
		if(!mPauseTimer.isExpired() && !mSlideTimer.isExpired())
			y = (mSlideTimer.getSquareRoot()-1);
		else if(!mPauseTimer.isExpired() && mSlideTimer.isExpired())
			y = 0;
		else
			y = (1-mSlideTimer.getSquare());
		//TODO FIX LEAK
		mImg.setColor(mColor.interpolate(quAbs<float>(y)).d);
		//mImg.draw((y)*G_SCREEN_WIDTH/2+G_SCREEN_WIDTH/2,G_SCREEN_HEIGHT/2,0,250);
		mImg.draw(G_SCREEN_WIDTH/2,y*G_SCREEN_HEIGHT/2+ G_SCREEN_HEIGHT/2,0,250);
	}
	virtual bool isFinished()
	{
		return mPauseTimer.isExpired() && mSlideTimer.getLinear() == 0;
	}
};

class QuShapeDrawer 
{
	QuBasicCamera mCamera;
public:
	QuShapeDrawer():mCamera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT){}

	void setRotation(GDeviceRotation aRot)
	{
		mCamera.setRotation(aRot);
	}
	void drawRectangle(QuBox2<int> aRect, QuColor aColor)
	{
		//draw from top left
		QuRectangleDrawObject<int> rect(aRect);
		rect.loadColor(aColor);
		glPushMatrix();
		mCamera.setScene();
		glTranslatef(-mCamera.getRotatedWidth()/2.0f,-mCamera.getRotatedHeight()/2.0f,0);
		rect.autoDraw();
		glPopMatrix();
	}
	//this draws rectangles in relative coordinates
	void drawRectangle(QuBox2<float> aRect, QuColor aColor)
	{
		QuVector2<int> dim(mCamera.getRotatedWidth(),mCamera.getRotatedHeight());
		QuBox2<int> rect(aRect.x * dim.x, aRect.y * dim.y, aRect.w * dim.x, aRect.h * dim.y);
		drawRectangle(rect,aColor);
	}
	void drawImageInRectangle(QuBox2<float> aRect, QuStupidPointer<QuBaseImage> aImg,  float alpha = 1, bool flipx = false, bool flipy = false)
	{
		QuVector2<int> dim(mCamera.getRotatedWidth(),mCamera.getRotatedHeight());
		QuBox2<int> rect(aRect.x * dim.x, aRect.y * dim.y, aRect.w * dim.x, aRect.h * dim.y);
		drawImageInRectangle(rect,aImg,alpha, flipx,flipy);
	}
	void drawImageInRectangle(QuBox2<int> aRect, QuStupidPointer<QuBaseImage> aImg, float alpha = 1, bool flipx = false, bool flipy = false)
	{
		//TODO flipx and flipy are broke, one needs to reflect the rectangle as well 
		QuImageDrawObject d;
		d.setImage(aImg);
		d.loadColor(QuColor(1,1,1,alpha));
		d.loadBoxVertices(aRect);
		glPushMatrix();
		mCamera.setScene();
		glDisable(GL_LIGHTING);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		/*
		if(flipx)
			glScalef(-1,1,1);
		if(flipy)
			glScalef(1,-1,1);*/
		glTranslatef(-mCamera.getRotatedWidth()/2.0f ,-mCamera.getRotatedHeight()/2.0f,0);
		
		d.autoDraw();
		glPopMatrix();
	}
};

//TODO needs to be able to draw axis
class QuGraphDrawer
{
	QuBasicCamera mCamera;
	QuDrawObject mAxis;
	QuDrawObject mGraph;
	bool mDrawAxis;
	QuBox2<float> mGraphBounds;
public:
	QuGraphDrawer():mCamera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,1),mDrawAxis(false),mGraphBounds(-1,-1,2,2)
	{
		mCamera.setRotation(G_DR_90);
	}

	//note, data must be reuploaded after setting new bounds
	void setHorizontalBounds(float aLeft, float aRight)
	{
		mGraphBounds.x = aLeft;
		mGraphBounds.w = aRight-aLeft;
	}
	void setVerticalBounds(float aTop, float aBot)
	{
		mGraphBounds.y = aBot;
		mGraphBounds.h = aTop - aBot;
	}
	void setDrawAxis(bool aDrawAxis)
	{
		aDrawAxis = mDrawAxis;
	}
	void setPoints(QuStupidPointer<float> aValues, float xTime)
	{
		float timeSum = 0;
		float * verts = new float[aValues.size() * 2];
		for(int i = 0; i < aValues.size(); i++)
		{
			float y = quClamp<float>(aValues[i],mGraphBounds.y,mGraphBounds.y+mGraphBounds.h);
			float x = quClamp<float>(timeSum,mGraphBounds.x,mGraphBounds.x+mGraphBounds.w);
			verts[i*2 + 0] = x;
			verts[i*2 + 1] = y;
			timeSum += xTime;
		}
		mGraph.setCount(aValues.size());
		mGraph.setDrawType(GL_LINE_STRIP);
		mGraph.loadColor(QuColor(0,0,0,1));
		mGraph.loadVertices(verts,2);
	}
	void draw()
	{
		//TODO draw axis
		mCamera.setRotation(G_DEVICE_ROTATION,G_RESTRICT_LANDSCAPE);
		glPushMatrix();
		mCamera.setScene();
		//glTranslatef(-mGraphBounds.w/4.0f,-mGraphBounds.h/4.0f,0);
		//translate graph to center
		glTranslatef(-(mGraphBounds.x+mGraphBounds.w/2.0f),-(mGraphBounds.y + mGraphBounds.h/2),0);
		glScalef(mCamera.getRotatedCamScreenRatio()/mGraphBounds.w,1/mGraphBounds.y,1);
		glDepthMask(false);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);

		mGraph.autoDraw();

		glEnable(GL_LIGHTING);
		glEnable(GL_DEPTH_TEST);
		glDepthMask(true);
		glPopMatrix();
	}
	QuDrawObject & getGraphDrawObject() { return mGraph; }
	QuDrawObject & getAxisDrawObject() { return mAxis; }
};

