#pragma once
#include "QuUtils.h"
#include "QuCamera.h"
#include "QuInterface.h"

class testApp
{
private:
	bool isMenu;
	int hasLoaded;
public:
	bool isDone;
	testApp():hasLoaded(0),isMenu(true)
	{
	}
	~testApp()
	{
	}
	void destroy()
	{
	}
	

	void drawDummyLoadingImage();
	void restartGame();

	void initialize();
	void setup();
	void update();
	void draw();

	void keyPressed  (int key);
	void keyReleased(int key);
	void mouseMoved(QuScreenCoord crd );
	void mouseDragged(QuScreenCoord crd, int button){}
	void mousePressed(QuScreenCoord crd, int button);
	void mouseReleased(QuScreenCoord crd, int button);

	//all hell break loose should this function be called
	//well actually, no, just make sure one resizes the QuCamera (and write some routines in QuCamera to do this)
	//TODO ^
	void windowResized(int w, int h);

	void tiltUpdate();
};

//put your own code in here! 
//TODO move out to its own .h and .cpp file
class QuExampleManager : public QuBaseManager
{
	bool mInit;
	QuStupidPointer<testApp> g;
public:
	QuExampleManager():mInit(false)
	{
	}
	virtual void initialize()
	{
		g = QuStupidPointer<testApp>(new testApp());
		g->setup();
		mInit = true;
	}
	virtual void update()
	{
		g->update();
		g->draw();
		//QuBasicCamera::setStaticProjection();
		//QuCoordinateSystemDrawer().draw();
	}
	virtual void keyDown(int key){}
	virtual void keyUp(int key){}
	virtual void singleDown(int button, QuScreenCoord crd){ g->mousePressed(crd,button);}
	virtual void singleUp(int button, QuScreenCoord crd){g->mouseReleased(crd,button);}
	virtual void singleMotion(QuScreenCoord crd){g->mouseMoved(crd);}
};