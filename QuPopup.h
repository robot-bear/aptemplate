#pragma once
#include "QuGameManager.h"
#include "QuGlobals.h

//TODO finish me
//this one is initialized by passing in the previous manager
//use the popup function to initialize
//use the popout function to cancel
//TODO test me, careful, risky island leaking business here
class QuPopup : QuBaseManager
{
	QuStupidPointer<QuBaseManager> mUnderManager;
public:
	QuPopup()
	{
		
	}
	bool isActiveManage()
	{
		return G_GAME_GLOBAL.getManager() == this;
	}
	void popup()
	{
		//can't have popup popping over itself, that would be bad
		assert(!isActiveManager());
		mUnderManager = G_GAME_GLOBAL.getManager();
		G_GAME_GLOBAL.setManager(QuStupidPointer<QuBaseManager>(this));
	}
	void popout()
	{
		G_GAME_GLOBAL.setManager(mUnderManager);
	}
	virtual void draw()
	{
		drawUnderManager();
	}
	void drawUnderManager(bool aGray)
	{
		mUnderManager->draw();
		if(aGray)
		{
			//TODO draw gray alpha box
		}
	}

	virtual void keyDown(int key){}
	virtual void keyUp(int key){}
	virtual void singleDown(int button, QuScreenCoord crd){}
	virtual void singleUp(int button, QuScreenCoord crd){}
	virtual void singleMotion(QuScreenCoord crd){}
};