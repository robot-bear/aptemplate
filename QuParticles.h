#pragma once
#include "QuInterface.h"
#include "QuCMath.h"
#include <map>
#include <list>

#define PMT std::map<QuStupidPointer<QuTintableGuiImage>,std::list<QuStupidPointer<QuConstantForceParticle> > >
//TODO should make QuConstantForceParticle a stupid pointer
//TODO should rewrite this using multimap if you can ever find a good reference.
//TODO write a QuEfficientParticleManager that preallocates memory for all particles and uses 
class QuParticleManager
{
	PMT mParts;
public:
	void addParticle(QuStupidPointer<QuTintableGuiImage> img, QuStupidPointer<QuConstantForceParticle> part)
	{
		mParts[img].push_back(part);
	}
	//slightly slower much less leak prone version
	void addParticle(QuStupidPointer<QuTintableGuiImage> img, QuConstantForceParticle & part)
	{
		addParticle(img,QuStupidPointer<QuConstantForceParticle>(new QuConstantForceParticle(part)));
	}
	void draw()
	{
		for(PMT::iterator it = mParts.begin(); it != mParts.end(); it++)
		{
			for(std::list<QuStupidPointer<QuConstantForceParticle> >::iterator jt = it->second.begin(); jt != it->second.end(); jt++)
			{
				if((*jt)->isUseColor)
					it->first->setColor((*jt)->getColor().d);
				//std::cout << (*jt)->x << " " << (*jt)->y << " " << (*jt)->rot << " " << (*jt)->s << std::endl;
				it->first->draw((*jt)->x,(*jt)->y,(*jt)->rot,(*jt)->s);
			}
		}
	}
	void update()
	{
		std::list<PMT::iterator> baseRemoval;
		for(PMT::iterator it = mParts.begin(); it != mParts.end(); it++)
		{
			std::list<std::list<QuStupidPointer<QuConstantForceParticle> >::iterator > removal;
			for(std::list<QuStupidPointer<QuConstantForceParticle> >::iterator jt = it->second.begin(); jt != it->second.end(); jt++)
			{
				(*jt)->update();
				if((*jt)->mTimer.isExpired())
					removal.push_back(jt);
				else if(!(*jt)->isInBox(QuBox2<int>(QuVector2<int>(0,0),G_GAME_GLOBAL.getRotatedScreenDimensions(G_DEVICE_ROTATION))))
					removal.push_back(jt);
			}
			for(std::list<std::list<QuStupidPointer<QuConstantForceParticle> >::iterator>::iterator jt = removal.begin(); jt != removal.end(); jt++)
				it->second.erase(*jt);
			if(it->second.size() == 0)
				baseRemoval.push_back(it);
		}
		for(std::list<PMT::iterator>::iterator it = baseRemoval.begin(); it != baseRemoval.end(); it++)
			mParts.erase((*it)->first);
	}
private:
};
#undef PMT