#pragma once
#include "GLES\gl.h"
#include "s3eSurface.h"
#include "s3eAccelerometer.h"
#include "s3eDebug.h"
#include <string>
#include <iostream>


static GLfloat NEUTRAL_COLOR[] = {1,1,1,1};
static GLfloat NASTY_COLOR[] = {0,0.5f,0,1};
static GLfloat DARK_RED_OPAQUE[] = {0.7f,0,0,1};


//drawing constants
//for our coordinate system
static GLfloat COORDINATE_SYSTEM_COORDS[6][3] = 
{
	0,0,0, 0,0,1,
	0,0,0, 0,1,0,
	0,0,0, 1,0,0
};
static GLfloat COORDINATE_SYSTEM_COLORS[6][4] =
{
	1,0,0,1, 1,0,0,1,
	0,1,0,1, 0,1,0,1,
	0,0,1,1, 0,0,1,1
};

//for gui
static GLfloat GUI_RECT_COORDS[4][3] = {-0.5,0.5,0,  0.5,0.5,0, -0.5,-0.5,0, 0.5,-0.5,0};
//static GLfloat GUI_RECT_COORDS[4][3] = {-0.5,-0.5,0,  0.5,-0.5,0, -0.5,0.5,0, 0.5,0.5,0};
static GLfloat GUI_RECT_UV[4][2] = {0,0,0,1,1,0,1,1};

//for transparency fading
static GLfloat FULL_OPAQUE[] = {1,1,1,1};
static GLfloat FULL_TRANSPARENT[] = {1,1,1,0};
static GLfloat HALF_OPAQUE[] = {1,1,1,0.5};
static GLfloat QUARTER_OPAQUE[] = {1,1,1,0.25};
static GLfloat THREE_QUARTER_OPAQUE[] = {1,1,1,0.75};
static GLfloat ONE_EIGTH_OPAQUE[] = {1,1,1,0.125};
static GLfloat FULL_RED_OPAQUE[] = {1,0,0,1};
static GLfloat FULL_BLUE_OPAQUE[] = {0,0,1,1};

//lighting constants
static GLfloat DEFAULT_LIGHT_AMBIENT[] = { 0.5f, 0.5f, 0.5f, 1 };
static GLfloat DEFAULT_LIGHT_DIFFUSE[] = { 0.4f, 0.4f, 0.4f, 1 };
static GLfloat DEFAULT_LIGHT_SPECULAR[] = { 0.1f, 0.1f, 0.1f, 1 };
static GLfloat DEFAULT_LIGHT_POSITION[] = { -0.20f, 0.75f, 3.0f, 0.0f };


//material constants
static const GLfloat DEFAULT_MAT_AMBIENT[] = { 0.2f,0.2f,0.2f,1.0f };
static const GLfloat DEFAULT_MAT_DIFFUSE[] = { 0.8f,0.8f,0.8f,1.0f };
static const GLfloat DEFAULT_MAT_SPECULAR[] = { 0.0f,0.0f,0.0f,1.0f };
static const GLfloat DEFAULT_MAT_EMISSION[] = { 0.0f,0.0f,0.0f,1.0f };
static const GLfloat DEFAULT_MAT_SHININESS = 0.0f;
