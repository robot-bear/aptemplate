#include "QuGlobals.h"
#include "QuGameManager.h"


//TODO this should cache the device rotation like it does with screen width and height...
//you can do this in airplay with its callbacks
GDeviceRotation QuGlobal::getDeviceRotation(){ return mRot; }

QuVector3<float>  QuGlobal::getAccel(GDeviceRotation rot)
{
	//TODO add support for other rotations
	quAssert(rot == G_DR_0);
	float y = s3eAccelerometerGetY()/1000.0f;
	float x = s3eAccelerometerGetX()/1000.0f;
	float z = s3eAccelerometerGetZ()/1000.0f;

	//TODO get rid of me
	if(s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION) == S3E_SURFACE_BLIT_DIR_ROT180)
	{
		x*=-1; y*=-1;
	}
	return QuVector3<float>(x,y,z);
}

void QuGlobal::setManager(QuStupidPointer<QuBaseManager> aMan)
{
	master = aMan;
}

void QuGlobal::setManagerAndInitialize(QuStupidPointer<QuBaseManager> aMan)
{
	aMan->initialize();
	master = aMan;
}

void QuGlobal::setAccel(QuVector3<float> aAccel){ mAccel = aAccel; }
void QuGlobal::setDeviceRotation(GDeviceRotation aRot){ mRot = aRot; }
void QuGlobal::setScreenSize(int aWidth, int aHeight){ mScreenWidth = aWidth; mScreenHeight = aHeight; }