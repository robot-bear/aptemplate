#pragma once
#include "QuGlobalConstants.h"
#include "QuUtils.h"
#include "QuClutils.h"


template<typename T, int N>
class QuVector
{
	T x[N];
	QuVector(){}
	QuVector(T _x[N]):x(_x)
	{}
};

template<typename T>
struct QuVector3
{
	T x,y,z;
	QuVector3(T _x = 0, T _y = 0, T _z = 0):x(_x),y(_y),z(_z){}
	QuVector3 & operator*=(T v){ x *= v, y *= v, z *= v; return *this; }
};

template<typename T>
struct QuFVector2;

template<typename T>
struct QuVector2
{
	T x,y;
	QuVector2():x(0),y(0){}
	QuVector2(const QuFVector2<T> & o){ x = o.x; y = o.y; }
	QuVector2(T _x, T _y):x(_x),y(_y){}
	bool operator==(const QuVector2 & o)
	{
		return o.x == x && o.y == y;
	}
	bool operator!=(const QuVector2 & o)
	{
		return o.x != x || o.y != y;
	}
	QuVector2<T> operator-(const QuVector2 & o)
	{
		return QuVector2<T>(o.x-x,o.y-y);
	}

	template <typename R> //usually R should be QuVector2 but this is open for inherited version...
	R getRotated(GDeviceRotation rot)
	{
		R r();
		switch(rot)
		{
		case G_DR_0:
			return *this; break;
		case G_DR_90:
			r.x = -y; r.y = x; break;
		case G_DR_180:
			r.x = -x; r.y = -y; break;
		case G_DR_270:
			r.x = y; r.y = -x; break;
		default:
			break;
		}
		return r;
	}

	QuVector2<T> getRotated(GDeviceRotation rot)
	{
		return getRotated<QuVector2<T> >(rot);
	}
};


template<typename T>
struct QuFVector2 : public QuVector2<T>
{
	QuFVector2(T _x = 0, T _y = 0):QuVector2<T>(_x,_y){}
	template<typename S>
	QuFVector2(const QuVector2<S> & o)
	{ QuVector2<T>::x = T(o.x); QuVector2<T>::y = T(o.y); }
	float magnitude()
	{
		return sqrt(QuVector2<T>::x*QuVector2<T>::x + QuVector2<T>::y*QuVector2<T>::y);
	}
	void normalize()
	{
		float m = magnitude();
		QuVector2<T>::x /= m; QuVector2<T>::y /= m;
	}
	QuFVector2<T> operator*(const float & s)
	{
		return QuFVector2<T>(s*QuVector2<T>::x, s*QuVector2<T>::y);
	}
	QuFVector2<T> operator+(const QuFVector2<T> & o)
	{
		return QuFVector2<T>(o.x+QuVector2<T>::x,o.y+QuVector2<T>::y);
	}
	QuFVector2<T> operator-(const QuFVector2<T> & o)
	{
		return QuFVector2<T>(o.x-QuVector2<T>::x,o.y-QuVector2<T>::y);
	}
	QuFVector2<T> & operator+=(const QuFVector2<T> & o)
	{
		QuVector2<T>::x += o.x;
		QuVector2<T>::y += o.y;
		return *this;
	}
	QuFVector2<T> & operator=(const QuFVector2<T> & o)
	{
		QuVector2<T>::x -= o.x;
		QuVector2<T>::y -= o.y;
		return *this;
	}
	QuFVector2<T> & operator-=(const QuFVector2<T> & o)
	{

		QuVector2<T>::x -= o.x;
		QuVector2<T>::y -= o.y;
		return *this;
	}
	QuFVector2<T> getRotated(GDeviceRotation rot) //here angle is CCW from <1,0>
	{
		//TODO switch to temptlate version
		//return QuVector2<T>::getRotated<QuFVector2<T> >(rot);

		QuFVector2<T> r();
		switch(rot)
		{
		case G_DR_0:
			return *this; break;
		case G_DR_90:
			r.x = -QuVector2<T>::y; r.y = QuVector2<T>::x; break;
		case G_DR_180:
			r.x = -QuVector2<T>::x; r.y = -QuVector2<T>::y; break;
		case G_DR_270:
			r.x = QuVector2<T>::y; r.y = -QuVector2<T>::x; break;
		default:
			break;
		}
		return r;
	}
	
	QuFVector2<T> getRotated(double angle) //here angle is CCW from <1,0>
	{
		QuFVector2<T> r;
		double ct = cos(angle);
		double st = sin(angle);
		r.x = QuVector2<T>::x*ct - QuVector2<T>::y*st;
		r.y = QuVector2<T>::x*st + QuVector2<T>::y*ct;
		return r;
	}
};

/*
template<typename T>
struct QuFVector2
{
	T x,y;
	QuFVector2(T _x, T _y):x(_x),y(_y){}
	bool operator==(const QuFVector2 & o)
	{
		return o.x == x && o.y == y;
	}
	bool operator!=(const QuFVector2 & o)
	{
		return o.x != x || o.y != y;
	}
	
	QuFVector2(float a)
	{ quAssert(a == 0); }
	template<typename S>
	QuFVector2(const QuVector2<S> & o)
	{ x = T(o.x); y = T(o.y); }
	float magnitude()
	{
		return sqrt(x*x + y*y);
	}
	void normalize()
	{
		float m = magnitude();
		x /= m; y /= m;
	}
	QuFVector2<T> operator-(const QuFVector2<T> & o)
	{
		return QuFVector2<T>(o.x-x,o.y-y);
	}
	QuFVector2<T> & operator+=(const QuFVector2<T> & o)
	{
		x += o.x;
		y += o.y;
		return *this;
	}
	QuFVector2<T> & operator-=(const QuFVector2<T> & o)
	{
		x -= o.x;
		y -= o.y;
		return *this;
	}
};*/

template<typename T>
struct QuBox2
{
	T x, y, w, h;
	QuBox2(T _x, T _y, T _w, T _h):x(_x),y(_y),w(_w),h(_h){}
	QuBox2(QuVector2<T> _p, QuVector2<T> _s):x(_p.x),y(_p.x),w(_s.x),h(_s.y){}
	bool doesContainPoint(QuVector2<T> pt)
	{
		if(pt.x > x+w || pt.x < x)
			return false;
		if(pt.y > y+h || pt.y < y)
			return false;
		return true;
	}
	bool doesContainCircle(QuVector2<T> pt, float r)
	{
		if( pt.x  + r < x ||
			pt.x - r > x+w ||
			pt.y + r < y ||
			pt.y - r > y+h)
			return false;
		return true;
	}

	//returns box with center relative to top left of aBox where aBox in [0,1]x[0,1] and w/h = to aDim (directProduct) aBox.w/h
	static QuBox2<T> getCenterRelBox(QuBox2<float> aBox, QuVector2<T> aDim)
	{
		T cx = aBox.x * aDim.x;
		T cy = aBox.y * aDim.y;
		T width = aBox.w * aDim.x;
		T height = aBox.h * aDim.y;
		return QuBox2<T>(cx - width/2, cy-height/2,width,height);
	}
};

/*
QuBox2<int> QuBox2(QuBox2<float> aBox, QuVector2<int> aTotal)
{
	return QuBox2<int>(aBox.x * aTotal.x, aBox.y * aTotal.y, aBox.w * aTotal.x, aBox.h * aTotal.y);
}*/


/* 
template<typename T>
struct QuPointInBox2 : QuBox2<T>
{
	QuPointInBox2(int _x, int _y, int _w, int _h):QuBox2<T>( _x,  _y,  _w,  _h){}
	QuVector2<T> getRotatedPoint(QuRotation rot)
	{
		QuVector2<T> r;

		//CLOCKWISE rotation
		switch(rot)
		{
		case G_DR_90:
			r = QuVector2<T>(y,w-x);
			break;
		case G_DR_180:
			r = QuVector2<T>(w-x,h-y);
			break;
		case G_DR_270:
			r = QuVector2<T>(h-y,x);
			break;
		default:
			r = QuVector2<T>(x,y);
			break;
		}
		return r;
	}
};*/


template<typename T>
struct QuPointInBox2
{
	int x, y, w, h;
	QuPointInBox2(int _x, int _y, int _w, int _h):x(_x),y(_y),w(_w),h(_h){}
	bool operator==(QuPointInBox2<T> & o){return x == o.x && y == o.y && w == o.w && h == o.h;}
	bool doesContainPoint(QuVector2<T> pt)
	{
		if(pt.x > x+w || pt.x < x)
			return false;
		if(pt.y > y+h || pt.y < y)
			return false;
		return true;
	}
	bool doesContainCircle(QuVector2<T> pt, float r)
	{
		if( pt.x  + r < x ||
			pt.x - r > x+w ||
			pt.y + r < y ||
			pt.y - r > y+h)
			return false;
		return true;
	}
	QuVector2<T> getRotatedPoint(QuRotation rot) const
	{
		QuVector2<T> r;

		//CLOCKWISE rotation
		switch(rot)
		{
		case G_DR_90:
			r = QuVector2<T>(y,w-x);
			break;
		case G_DR_180:
			r = QuVector2<T>(w-x,h-y);
			break;
		case G_DR_270:
			r = QuVector2<T>(h-y,x);
			break;
		default:
			r = QuVector2<T>(x,y);
			break;
		}
		return r;
	}
};

struct QuScreenCoord
{
	QuPointInBox2<int> mScreenBox;
	QuScreenCoord():mScreenBox(0,0,0,0){}
	QuScreenCoord(int _x, int _y, int _w, int _h):mScreenBox(_x,_y,_w,_h){}
	bool operator==(int a){ return mScreenBox.w == 0 && mScreenBox.h == 0; }
	bool operator==(QuScreenCoord & o){ return o.mScreenBox == mScreenBox; }

	//TODO consider not having the rotation parameter here
	QuVector2<int> getIPosition(GDeviceRotation rot = G_DR_0) const
	{
		return mScreenBox.getRotatedPoint(rot);
	}
	QuFVector2<float> getFPosition(GDeviceRotation rot = G_DR_0) const
	{
		QuFVector2<float> pt(mScreenBox.getRotatedPoint(rot));
		if(rot == G_DR_90 || rot == G_DR_270)
		{
			pt.x /= mScreenBox.h;
			pt.y /= mScreenBox.w;
		}
		else
		{
			pt.x /= mScreenBox.w;
			pt.y /= mScreenBox.h;
		}
		return pt;
	}
	QuScreenCoord & reflect(GScreenReflection aRef)
	{
		if(aRef & G_SCREEN_REFLECT_VERTICAL)
			mScreenBox.y = mScreenBox.h-mScreenBox.y;
		else if(aRef & G_SCREEN_REFLECT_HORIZONTAL)
			mScreenBox.x = mScreenBox.w-mScreenBox.x;
			
		return *this;
	}
	//this returns QuScreenCoord cropped to cotaining rectangle inside of QuScreenCoord
	QuScreenCoord crop(QuBox2<float> aBox)
	{
		return crop(QuBox2<int>(aBox.x * mScreenBox.w,aBox.y * mScreenBox.h, aBox.w * mScreenBox.w, aBox.h * mScreenBox.h));
	}
	QuScreenCoord crop(QuBox2<int> aBox)
	{
		return QuScreenCoord(mScreenBox.x-aBox.x,mScreenBox.y-aBox.y,aBox.w,aBox.h);
	}
	//this function tells us whether this is contained in aBox
	//TODO test these
	bool doesBoxContain(QuBox2<float> aBox, GDeviceRotation aRot = G_DR_0) const
	{
		QuScreenCoord o(*this);
		o.rotate(aRot);
		return aBox.doesContainPoint(QuVector2<float>(o.mScreenBox.x/(float)mScreenBox.w, o.mScreenBox.y/(float)mScreenBox.h));
	}
	bool doesBoxContain(QuBox2<int> aBox, GDeviceRotation aRot = G_DR_0) const
	{
		QuScreenCoord o(*this);
		o.rotate(aRot);
		return aBox.doesContainPoint(QuVector2<int>(o.mScreenBox.x,o.mScreenBox.y));
	}
	QuScreenCoord & rotate(GDeviceRotation rot)
	{
		QuVector2<int> pos = getIPosition(rot);
		if(rot == G_DR_90 || rot == G_DR_270)
			quSwap(mScreenBox.w,mScreenBox.h);
		mScreenBox.x = pos.x;
		mScreenBox.y = pos.y;
		return *this;
	}
	//aOther - *this
	QuFVector2<float> getFChange(const QuScreenCoord & aOther) const
	{
		QuFVector2<float> other(aOther.getFPosition());
		QuFVector2<float> me(getFPosition());
		return other - me;
	}
	QuVector2<int> getIChange(const QuScreenCoord & aOther) const
	{
		QuVector2<int> other(aOther.getIPosition());
		QuVector2<int> me(getIPosition());
		return other - me;
	}
};

