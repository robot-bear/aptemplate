#pragma once
#include "IwGl.h"
#include "QuClutils.h"
#include "QuCMath.h"
#include "QuImage.h"
#include "QuModels.h"

template<class T>
struct QuArrayPointer
{
	bool mInit;
	size_t mSize;

	T * mData;
	int mStride;
	int mStart;
	GLenum mType;
	int mDim;

public:
	QuArrayPointer()
	{
		mInit = false;
		mSize = sizeof(T);
	}
	QuArrayPointer(T * aData, int aStride = 0, int aStart = 0)
	{
		initialize(aData,aStride,aStart);
	}

	//copy constructor is retarded but needed for using QuArrayPointer in reference maps, we just make sure the copy constructor is not used on initialized QuArrayPointers
	QuArrayPointer(const QuArrayPointer & o)
	{
		quAssert(!o.mInit);
		mInit = false;
		mSize = sizeof(T);
	}
	~QuArrayPointer()
	{
		deInit();
	}
	QuArrayPointer & operator=(const QuArrayPointer & o)
	{
		deInit();
		mInit = o.mInit; mSize = o.mSize; mData = o.mData; mStride = o.mStride; mStart = o.mStart; mType = o.mType; mDim = o.mDim;
		return *this;
	}
	/* TODO get rid of me
	QuArrayPointer & operator=(QuArrayPointer o)
	{
		deInit();
		mInit = o.mInit; mSize = o.mSize; mData = o.mData; mStride = o.mStride; mStart = o.mStart; mType = o.mType; mDim = o.mDim;
		return *this;
	}*/
	bool isInit()
	{
		return mInit;
	}
	bool initialize(T * aData, GLenum aType, int aDim, int aStride = 0, int aStart = 0)
	{
		if(mInit)
			deInit();
		if(!checkType(aType))
			return false;
		mData = aData;
		mStride = aStride;
		mStart = aStart;
		mType = aType;
		mDim = aDim;
		mInit = true;
		return true;
	}
	void deInit()
	{
		if(!mInit)
			return;
		delete [] mData;
		mData = NULL;
		mInit = false;
	}
	bool checkType(GLenum aType)
	{
		switch(aType)
		{
		case GL_TEXTURE_COORD_ARRAY:
			break;
		case GL_VERTEX_ARRAY:
			break;
		case GL_NORMAL_ARRAY:
			break;
		case GL_COLOR_ARRAY:
			break;
		default:
			return false;
		}
		return true;
	}

	void enable()
	{
		
		if(!mInit)
			return;
		glEnableClientState(mType);
		switch(mType)
		{
		case GL_TEXTURE_COORD_ARRAY:
			glTexCoordPointer(mDim,GL_FLOAT,mSize*mStride,mData+mStart);
			break;
		case GL_VERTEX_ARRAY:
			glVertexPointer(mDim,GL_FLOAT,mSize*mStride,mData+mStart);
			break;
		case GL_NORMAL_ARRAY:
			glNormalPointer(GL_FLOAT,mSize*mStride,mData+mStart);
			break;
		case GL_COLOR_ARRAY:
			glColorPointer(mDim,GL_FLOAT,mSize*mStride,mData+mStart);
			break;
		default:
			break;
		}

	}
	void disable()
	{
		if(!mInit)
			return;
		glDisableClientState(mType);
	}
};

class QuDrawObject
{
	int mCount;
	QuArrayPointer<float> mV;	//vertex
	QuArrayPointer<float> mT;	//texture
	QuArrayPointer<float> mN;	//normal
	QuArrayPointer<float> mC;	//colors

	bool mUseImage;
	QuStupidPointer<QuBaseImage> mImage;
	GLenum mDrawType;
	
public:
	QuDrawObject()
	{
		mCount = 0;
		mUseImage = false;
		mDrawType = GL_TRIANGLE_STRIP;
	}
	virtual ~QuDrawObject(){}
	virtual void deinit()
	{
		mV.deInit();
		mT.deInit();
		mN.deInit();
		mC.deInit();
		mUseImage = false;
		mImage.setNull();
		mDrawType = GL_TRIANGLE_STRIP;
	}
	int getCount()
	{
		return mCount;
	}
	void setCount(int aCount)
	{
		mCount = aCount;
	}
	void setDrawType(GLenum aType)
	{
		//TODO check if draw type is valid
		mDrawType = aType;
	}
	template<int SZ>
	void loadVertices(const ARRAYN<float,SZ> aData, int dim = 3)
	{
		float * verts = new float[SZ];
		memcpy(verts,aData.d,SZ*sizeof(float));
		mV.initialize(verts,GL_VERTEX_ARRAY,dim);
	}
	void loadVertices(float * aData = NULL, int dim = 3)
	{
		if(aData == NULL)
			mC.deInit();
		else
			mV.initialize(aData,GL_VERTEX_ARRAY,dim);
	}
	void loadVerticesAndGenerateNormals(float * aData)
	{
		mV.initialize(aData,GL_VERTEX_ARRAY,3);

		//generate vertices
		if(mCount > 0)
				mN.initialize(makeNormalArrayFromTriangleStrip(aData,mCount),GL_NORMAL_ARRAY,3);
		
	}

	template<int SZ>
	void loadNormals(const ARRAYN<float,SZ> aData)
	{
		float * norms = new float[SZ];
		memcpy(norms,aData.d,SZ*sizeof(float));
		mN.initialize(norms,GL_NORMAL_ARRAY,3);
	}

	void loadNormals(float * aData = NULL)
	{
		if(aData == NULL)
			mN.deInit();
		else
			mN.initialize(aData,GL_NORMAL_ARRAY,3);
	}

	template<int SZ>
	void loadTexCoords(const ARRAYN<float,SZ> aData)
	{
		float * tex = new float[SZ];
		memcpy(tex,aData.d,SZ*sizeof(float));
		mT.initialize(tex,GL_TEXTURE_COORD_ARRAY,2);
	}

	void loadTexCoords(float * aData)
	{
		if(aData == NULL)
			mT.deInit();
		else
			mT.initialize(aData,GL_TEXTURE_COORD_ARRAY,2);
	}
	void loadColors(float * aData = NULL, int size = 4)
	{
		if(aData == NULL)
			mC.deInit();
		else
			mC.initialize(aData,GL_COLOR_ARRAY,size);
	}
	template<int SZ> 
	void loadColors(ARRAYN<float,SZ> aData, int size = 4)
	{
		float * colors = new float[SZ];
		memcpy(colors,aData.d,SZ*sizeof(float));
		mC.initialize(colors,GL_COLOR_ARRAY,size);
	}
	void loadColor(QuColor aColor)
	{
		float * colors = new float[getCount()*4];
		for(int i = 0; i < getCount(); i++)
		{
			colors[4*i + 0] = aColor.r;
			colors[4*i + 1] = aColor.g;
			colors[4*i + 2] = aColor.b;
			colors[4*i + 3] = aColor.a;
		}
		loadColors(colors);
	}

	bool loadImage(std::string filename, float * aTexData, int aTexStride = 0, int aTexStart = 0);
	//TODO seperate this out into a loadTExture function in case we want to do our own image binding
	bool loadImage(QuStupidPointer<QuBaseImage> aImage, float * aTexData = NULL, int aTexStride = 0, int aTexStart = 0);
	void unloadImage()
	{
		if(!mImage.isNull())
		{
			mT.deInit();
			mImage.setNull();
		}
	}
	QuStupidPointer<QuBaseImage> getImage()
	{ 
		return mImage;
	}
	bool isTextureLoaded()
	{
		//if we are not using images
		if(!mUseImage)
		{
			//if we still have a texture return true
			if(mT.isInit())
				return true;
			else return false;
		}
		else
		{
			return mImage->isImageLoaded();
		}
	}
	virtual void enable()
	{
		//this means we are using a QuImage
		if(mUseImage)
			if(!mImage->bind()) //do not draw if image is not finished loading
				return;
		

		mV.enable();
		mT.enable();
		mN.enable();
		mC.enable();
	}

	virtual void disable()
	{
		if(mUseImage)
			mImage->unbind();

		mV.disable();
		mT.disable();
		mN.disable();
		mC.disable();
	}
	void autoDraw()
	{
		enable();
		draw();
		disable();
	}
	void draw()
	{
		//TODO get rid of me
		quAssert(mCount > 0); quAssert(mV.isInit());
		if(mCount > 0 && mV.isInit())
			glDrawArrays(mDrawType,0,mCount);
	}
	float getImageWidthToHeightRatio()
	{
		if(!mUseImage)
			return 0;
		return mImage->getWidthToHeightRatio();
	}
	void enableTexture(){mT.enable();}
	void enableVertex(){mV.enable();}
	void enableNormal(){mN.enable();}
	void enableColor(){mC.enable();}
private:
};


class QuImageDrawObject : public QuDrawObject
{
	bool mIsImageSet;
public:
	QuImageDrawObject():mIsImageSet(false){}
	virtual ~QuImageDrawObject(){}
	//TODO useimagecoords and normalisze height are broken
	bool setImage(QuStupidPointer<QuBaseImage> aImage, bool useImageCoords = false, bool aNormalizeHeight = false)
	{
		bool flag = loadImage(aImage);
		if(flag)
		{
			if(!useImageCoords)
			{
				float * rectCoords = new float[4*3];
				memcpy(rectCoords,GUI_RECT_COORDS,sizeof(float) * 4 * 3);
				loadVertices(rectCoords);
				mIsImageSet = true;
				return true;
			}
			else
			{
				int w = aImage->getWidth();
				int h = aImage->getHeight();
				if(aNormalizeHeight)
				{
					w/=h;
					h/=h;
				}
				loadBoxVertices(QuBox2<int>(-w/2,h/2,w,h));
			}
		}
		return false;
	}
	template<typename T>
	void loadBoxVertices(const QuBox2<T> & aRect)
	{
		float x = aRect.x;	float y = aRect.y;
		float w = aRect.w;  float h = aRect.h;
		float verts[] = {x,y+h,0,x+w,y+h,0,x,y,0,x+w,y,0};
		QuDrawObject::loadVertices<12>(ARRAYN<float,12>(verts));
	}

};

//TODO this should keep key map of ARRAYNs and upload it using setColor
template<typename T>
class QuColorableDrawObject : public QuImageDrawObject
{
	std::map<T,QuArrayPointer<float> > mColorMap;
	T mKey;
public:
	/*
	QuColorableDrawObject<T> & operator=(QuColorableDrawObject<T> o)
	{
		mColorMap = o.mColorMap;
		mKey = o.mKey;
		return *this;
	}*/
	void addColor(T aKey, float * aColor, int size = 4)
	{
		if(mColorMap.size() == 0)
			mKey = aKey;
		mColorMap.erase(aKey);
		mColorMap[aKey] = QuArrayPointer<float>();
		mColorMap[aKey].initialize(aColor,GL_COLOR_ARRAY,size);
	}
	template<int SZ> 
	void addColor(T aKey, ARRAYN<float,SZ> aData, int size = 4)
	{
		float * colors = new float[SZ];
		memcpy(colors,aData.d,SZ*sizeof(float));
		addColor(aKey,colors,size);
	}
	void clearColors()
	{
		mColorMap.clear();
	}

	void setKey(T aKey)
	{
		//if(mColorMap.find(aKey) == mColorMap.end())
		//	cout << "ERROR, SETTING INVALID KEY TO COLOR MAP " << endl; return;
		mKey = aKey;
	}

	bool isColorsExist(){ return mColorMap.size() > 0; }

	virtual void enable()
	{
		QuDrawObject::enable();
		if(isColorsExist())
			mColorMap[mKey].enable();
	}
	virtual void disable()
	{
		QuDrawObject::disable();
		if(isColorsExist())
			mColorMap[mKey].disable();
	}
};

template<typename T>
class QuRectangleDrawObject : public QuDrawObject
{
	QuBox2<T> mRect;
public:
	QuRectangleDrawObject(const QuBox2<T> & aRect):mRect(aRect)
	{
		setCount(4);
		updateArrays();
	}
	void loadBoxVertices(const QuBox2<T> & aRect)
	{
		float x = aRect.x;	float y = aRect.y;
		float w = aRect.w;  float h = aRect.h;
		float verts[] = {x,y+h,0,x+w,y+h,0,x,y,0,x+w,y,0};
		QuDrawObject::loadVertices<12>(ARRAYN<float,12>(verts));
	}
private:
	void updateArrays()
	{
		loadBoxVertices(mRect);
	}

};

class QuCoordinateSystemDrawer
{
	QuColorableDrawObject<char> mCoords;
public:
	QuCoordinateSystemDrawer()
	{
		mCoords.setCount(6);
		float * verts = new float[6*3];
		float * colors = new float[6*4];
		memcpy(verts,COORDINATE_SYSTEM_COORDS,sizeof(float)*6*3);
		memcpy(colors,COORDINATE_SYSTEM_COLORS,sizeof(float)*6*4);
		mCoords.loadVertices(verts);
		mCoords.addColor(0,colors,4);
		mCoords.setKey(0);
		mCoords.setDrawType(GL_LINES);
	}
	void draw()
	{
		glPushMatrix();
		glScalef(200,200,200);
		glDisable(GL_LIGHTING);
		mCoords.autoDraw();
		glEnable(GL_LIGHTING);
		glPopMatrix();
	}
};


//mimics immediete mode functionality
class QuImmedieteModeDrawer
{
	struct Triple
	{
		float x,y,z;
		Triple(){}
		Triple(float _x, float _y, float _z):x(_x),y(_y),z(_z){}
	};
	struct Quadruple
	{
		float x,y,z,w;
		Quadruple(){}
		Quadruple(float _x, float _y, float _z, float _w):x(_x),y(_y),z(_z),w(_w){}
	};
	QuColor mColor;
	std::vector<Triple> mVerts;
	std::vector<Quadruple> mColors;
	GLenum mStyle;
	float mLineWidth;
	float mPointWidth;
public:
	QuImmedieteModeDrawer():mLineWidth(1),mPointWidth(1),mStyle(GL_TRIANGLE_STRIP){}
	void setStyle(GLenum aStyle){mStyle = aStyle;}
	void setLineWidth(float aLineWidth){mLineWidth = aLineWidth;}
	void setPointWidth(float aPointWidth){mPointWidth = aPointWidth;}
	void color4f(QuColor aColor)
	{
		mColor = aColor;
	}
	void vertex3f(float x, float y, float z = 0)
	{
		mColors.push_back(Quadruple(mColor.r,mColor.g,mColor.b,mColor.a));
		mVerts.push_back(Triple(x,y,z));
	}
	void begin()
	{
		mColors.clear();
		mVerts.clear();
	}
	void draw()
	{
		QuDrawObject mDraw;
		mDraw.setCount(mVerts.size());
		if(mVerts.size() == 0)
			return;
		float * verts = new float[mVerts.size() * 3];
		float * colors = new float[mColors.size() * 4];
		memcpy(verts,&mVerts[0],mVerts.size() * 3 * sizeof(float));
		memcpy(colors,&mColors[0],mColors.size() * 4 * sizeof(float));
		mDraw.loadVertices(verts);
		mDraw.loadColors(colors);
		mDraw.setDrawType(mStyle);
		glLineWidth(mLineWidth);
		glPointSize(mPointWidth);
		mDraw.autoDraw();
	}
	void end()
	{
		draw();
	}
};