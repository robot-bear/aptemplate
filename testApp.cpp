#include "testApp.h"
#include "QuInterface.h"
#include "QuSoundManager.h"
#include "QuConstants.h"
#include "s3eKeyboard.h"
//--------------------------------------------------------------

void testApp::initialize()
{
}
void testApp::setup(){
}

void testApp::restartGame()
{
	isMenu = true;
}


void testApp::drawDummyLoadingImage()
{
	QuTintableGuiImage img;
	img.loadImage(std::string("images/image.png"));
	img.draw();
}

void testApp::update()
{
	if(hasLoaded == 1)
	{
		initialize(); hasLoaded++;
	}
	else if(hasLoaded == 2)
	{
	}
}

void testApp::draw(){
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
	if(hasLoaded == 0)
	{
		drawDummyLoadingImage(); 
		hasLoaded++;
		return;
	}
	drawDummyLoadingImage(); 
}

void testApp::keyPressed(int key){
}

void testApp::keyReleased(int key){
	if(key == s3eKeyAbsBSK)
	{
		restartGame();
	}
}
 
void testApp::mousePressed(QuScreenCoord crd, int button)
{
	QuVector2<int> p = crd.getIPosition();
}

void testApp::mouseReleased(QuScreenCoord crd, int button)
{
	QuVector2<int> p = crd.getIPosition();
}

//TODO this has changed
void testApp::mouseMoved(QuScreenCoord crd)
{
		QuVector2<int> p = crd.getIPosition();
}


//--------------------------------------------------------------
void testApp::windowResized(int w, int h){
	//reset screen center
}

void testApp::tiltUpdate()
{
	float y = s3eAccelerometerGetY()/1000.0;
	float x = s3eAccelerometerGetX()/1000.0;
	if(s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION) == S3E_SURFACE_BLIT_DIR_ROT180)
	{
		x*=-1; y*=-1;
	}
}
