#pragma once
#include <string>
#include <map>
#include "QuUtils.h"
#include "s3eSound.h"
#include "s3eFile.h"
#include "s3eMemory.h"
#include <iostream>
#include "QuClutils.h"




class QuSoundStructInterface
{
protected:
	QuTimer mVolumeTimer;
	float mMaxVolume;
	float mMinVolume;
	float mVolume;
	bool mLoop;
public:
	QuSoundStructInterface(bool aLoop)
	{
		mLoop = aLoop;
		mMaxVolume = 1;
		mMinVolume = 0;
		mVolume = 1;
	}
	virtual ~QuSoundStructInterface(){};
	virtual void play() = 0;
	virtual bool isPlaying() = 0;
	virtual void stop() = 0;
	virtual void setVolume(float aVolume)
	{
		mVolumeTimer = QuTimer();
		mVolume = aVolume;
	}
	virtual void update()
	{
		float t = mVolumeTimer.getLinear();
		setVolume(getVolume());
		mVolumeTimer++;
	}
	float getVolume()
	{
		if(mVolumeTimer.isSet())
		{
			float t = mVolumeTimer.getLinear();
			return (mMinVolume*(1-t) + mMaxVolume*t);
		}
		else 
			return mVolume;
	}
	void setFade(int time, float max)
	{
		if(mVolumeTimer.isSet())
			mMinVolume = getVolume();
		else mMinVolume = 0;
		mMaxVolume = max;
		mVolumeTimer.setTargetAndReset(time);
	}
	bool isFadingOut()
	{
		return mMaxVolume <= mMinVolume;
	}
	void setLoop(bool looping)
	{
		mLoop = looping;
	}
};


class QuApRawSoundStruct : public QuSoundStructInterface
{
	bool mMultiPlay;
	int mChannel;
	void * mSoundData;
	int32 mFileSize;
	int mHz;
	void loadSound(std::string filename)
	{
		//load the sound
		s3eFile *fileHandle = s3eFileOpen(filename.c_str(), "rb");
			//TODO error in loading??
		mFileSize = s3eFileGetSize(fileHandle);

		//TODO whats wrong here
		if(mFileSize == -1)
			std::cout << "failed to open " << filename << std::endl;
		mSoundData = new char[mFileSize];
		//memset(mSoundData, 0, mFileSize);
		s3eFileRead(mSoundData, mFileSize, 1, fileHandle);
			//TODO error in reading??
		s3eFileClose(fileHandle);
	}
public:
	static int32 OnEndOfSample(void * _systemData, void* userData)
	{
		return 0;
	}
	QuApRawSoundStruct(std::string filename, int aHz, bool stream, bool aLoop, bool multiplay):QuSoundStructInterface(aLoop)
	{
		loadSound(filename);
		mHz = aHz;
		//currently only multiplay supported
		mMultiPlay = true;
		mChannel = -1;
	}
	~QuApRawSoundStruct()
	{
		delete [] mSoundData;
	}
	void setRate(int aHz)
	{ mHz = aHz; }
	int getRate()
	{ return mHz; }
	void play()
	{
		//try and get a free mChannel
		//TODO should use s3eSoundChannelRegister with S3E_CHANNEL_END_SAMPLE so we know when the sound has finished playing
		mChannel =  s3eSoundGetFreeChannel();
		if(mChannel == -1)
		{
			std::cout << " no channel found " << std::endl;
			return;
		}
		int repeat = 1;
		if(mLoop)
			repeat = 0;

		s3eSoundChannelSetInt(mChannel,S3E_CHANNEL_RATE,mHz);
		s3eSoundChannelSetInt(mChannel,S3E_CHANNEL_VOLUME,IW_FIXED_TO_FLOAT(S3E_SOUND_MAX_VOLUME*IW_FIXED_FROM_FLOAT(getVolume())));
		s3eSoundChannelPlay(mChannel,(int16 *)mSoundData,mFileSize/2,repeat,0);
	}
	bool isPlaying()
	{
		if(mChannel == -1)
			return false;
		return s3eSoundChannelGetInt(mChannel,S3E_CHANNEL_STATUS) == 1;
	}
	void stop()
	{
		if(mChannel != -1)
			s3eSoundChannelStop(mChannel);
	}
	/* delete me
	float getVolume()
	{
		float t = mVolumeTimer.getLinear();
		return (mMinVolume*(1-t) + mMaxVolume*t);
	}
	void setVolume(float aVolume)
	{
		if(mChannel != -1)
			s3eSoundChannelSetInt(mChannel,S3E_CHANNEL_VOLUME,getVolume()*S3E_SOUND_MAX_VOLUME);
	}*/
};

class QuSoundManagerInterface
{
public:
	virtual ~QuSoundManagerInterface(){}
	virtual bool areSoundsPlaying() = 0;
	virtual QuStupidPointer<QuSoundStructInterface> loadSound(std::string filename) = 0;
	virtual QuStupidPointer<QuSoundStructInterface> getSound(std::string filename) = 0;
	virtual void playSound(std::string filename){getSound(filename)->play();}
	virtual void deleteSound(std::string filename) = 0;
	virtual void safeDeleteSound(std::string filename){deleteSound(filename);}	//may or may not delete sound depending if it is still being used or not
	virtual void update(){}
};



template<int RATE = 44100>
class QuApRawSoundManager : public QuSoundManagerInterface
{
	std::map<std::string,QuStupidPointer<QuApRawSoundStruct> > mSounds;
public:
	int getRate(){ return RATE; }
	QuApRawSoundManager()
	{
	}
	~QuApRawSoundManager()
	{
		for(std::map<std::string,QuStupidPointer<QuApRawSoundStruct> >::iterator i = mSounds.begin(); i != mSounds.end(); i++)
			i->second.setNull();
	}
	bool areSoundsPlaying()
	{
		for(std::map<std::string,QuStupidPointer<QuApRawSoundStruct> >::iterator i = mSounds.begin(); i != mSounds.end(); i++)
			if(i->second->isPlaying())
				return true;
		return false;
	}
	void safeDeleteSound(std::string filename)
	{
		if(mSounds.find(filename) != mSounds.end())
			if(mSounds[filename].getReferenceCount() == 1)
			{
				mSounds[filename]->stop();
				mSounds.erase(filename);
			}
	}
	void deleteSound(std::string filename)
	{
		quAssert(mSounds[filename].getReferenceCount() == 1);
		mSounds.erase(filename);
	}
	QuStupidPointer<QuSoundStructInterface> loadSound(std::string filename, int aHz, bool stream = false, bool loop = false, bool multiplay = false)
	{
		//if found, return it
		if (mSounds.find(filename) != mSounds.end())
			return mSounds[filename].safeCast<QuSoundStructInterface>();
		//otherwise load it
		mSounds[filename] = QuStupidPointer<QuApRawSoundStruct>(new QuApRawSoundStruct(filename, aHz, stream,loop,multiplay));
		return mSounds[filename].safeCast<QuSoundStructInterface>();
	}
	QuStupidPointer<QuSoundStructInterface> loadSound(std::string filename)
	{
		return loadSound(filename,RATE);
	}
	QuStupidPointer<QuSoundStructInterface> getSound(std::string filename)
	{
		return loadSound(filename);
		//whybother
		if (mSounds.find(filename) == mSounds.end())
			return QuStupidPointer<QuSoundStructInterface>(NULL);
		return mSounds[filename].safeCast<QuSoundStructInterface>();
	}
	void playSound(std::string filename,bool looping = false)
	{
		if (mSounds.find(filename) != mSounds.end())
		{
			mSounds[filename]->play();
			mSounds[filename]->setLoop(looping);
		}
	}
	void stopSound(std::string filename)
	{
		if (mSounds.find(filename) != mSounds.end())
			mSounds[filename]->stop();
	}

	void update()
	{
		for(std::map<std::string,QuStupidPointer<QuApRawSoundStruct> >::iterator i = mSounds.begin(); i != mSounds.end(); i++)
			i->second->update();
	}
	void setFade(std::string filename, bool isFadeIn)
	{
		if (mSounds.find(filename) != mSounds.end())
		{
			if(isFadeIn)
				mSounds[filename]->setFade(20,1);
			else
				mSounds[filename]->setFade(20,0);
		}
	}
	bool isFadingOut(std::string filename)
	{
		if (mSounds.find(filename) != mSounds.end())
			return mSounds[filename]->isFadingOut();
	}	
};