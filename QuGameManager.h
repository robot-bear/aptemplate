#pragma once
#include "QuCMath.h"

//manager manages all events to the game itself
class QuBaseManager
{
public:
	virtual ~QuBaseManager(){}
	virtual void initialize(){}
	virtual void update(){}

	virtual void keyDown(int key){}
	virtual void keyUp(int key){}

	virtual void singleDown(int button, QuScreenCoord crd){}
	virtual void singleUp(int button, QuScreenCoord crd){}
	virtual void singleMotion(QuScreenCoord crd){}
	//TODO figure out what type of parametrs we want in here
	virtual void singleDeltaMotion(){}

	virtual void screenResize(int w, int h){}

	virtual void multiTouch(int button, bool state,  QuScreenCoord crd){}
	virtual void multiMotion(int button,  QuScreenCoord crd){}
};