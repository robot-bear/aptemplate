#include "QuGlobals.h"
#include "QuCamera.h"
#include "QuImage.h"

void ExDrawImageToFitScreenLandscape(QuStupidPointer<QuBaseImage> aImg)
{
	QuBasicCamera camera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT);
	camera.setRotation(G_DR_90);
	camera.setRotation(G_DEVICE_ROTATION,G_RESTRICT_LANDSCAPE);
	QuImageDrawObject d;
	d.setImage(aImg);

	glPushMatrix();
	camera.setScene();

	//TODO get rid of me and use your qucontext class
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glDepthMask(false);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_LIGHTING);

	glScalef(camera.getRotatedWidth(),camera.getRotatedHeight(),1);
	d.autoDraw();
	glPopMatrix();
}