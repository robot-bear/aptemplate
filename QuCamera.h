#pragma once
#include "QuConstants.h"
#include "QuUtils.h"
#include "QuMath.h"
#include "QuGlobals.h"
#include "QuCMath.h"

static const float DEFAULT_CAMERA_DISTANCE = 500.0f;
static const float DEFAULT_CAMERA_HEIGHT = 1.0f;

//our GL context DOES NOT control surface rotation so the surface is always based on the starting width and height of the surface
//note that even when rotated, the width and height of the surface as reported by G_GLOBAL will remain the same


//TODO this is inconsistent with the mouse coordinates
//you should make a cat camera version which is the wrong version and then fix this version.
//TODO is the remark above still relevant???

//if requested dimension are too large, it will draw outside the surface
class QuBasicCamera
{

protected:
	float mScale;

	//this is distance in GL units from top to bottom of viewport surface this camera is drawing on
	float mTargetHeight;
	float mCameraDistance; 

	float mCamScreenBaseWidth;
	float mCamScreenBaseHeight;
	//these are the rotated dimensions
	float mCamScreenRotatedWidth;
	float mCamScreenRotatedHeight;

	int mRot;
private:
	void setParameters(int aCamScreenWidth,int aCamScreenHeight)
	{
		mCamScreenRotatedWidth = aCamScreenWidth;
		mCamScreenRotatedHeight = aCamScreenHeight;
		
	}
	
public:
	//setting aCamScreenHeight will stretch the screen
	QuBasicCamera(int aCamScreenWidth,int aCamScreenHeight = -1, float aTargetHeight = DEFAULT_CAMERA_HEIGHT, float aDistance = DEFAULT_CAMERA_DISTANCE)
	{
		mRot = 0;
		mCamScreenBaseWidth = aCamScreenWidth;
		if(aCamScreenHeight == -1)
			mCamScreenBaseHeight = aCamScreenWidth * G_SCREEN_HEIGHT / (float)G_SCREEN_WIDTH;
		else
			mCamScreenBaseHeight = aCamScreenHeight;

		mCameraDistance = aDistance;
		mTargetHeight= aTargetHeight;
		setParameters(mCamScreenBaseWidth,mCamScreenBaseHeight);
	}
	virtual ~QuBasicCamera(){};
	
	float getDrawingSurfaceHeight(){ return mTargetHeight; }
	float getDrawingSurfaceWidth(){ return mTargetHeight * getBaseCamScreenRatio(); }

	float getRotatedWidth(){return mCamScreenRotatedWidth;}
	float getRotatedHeight(){return mCamScreenRotatedHeight;}
	float getRotatedCamScreenRatio()
	{ return mCamScreenRotatedWidth/mCamScreenRotatedHeight; }


	//TODO
	//private:
	float getBaseCamScreenWidth(){return mCamScreenBaseWidth; }
	float getBaseCamScreenHeight(){return mCamScreenBaseHeight; }
	float getBaseCamScreenRatio()
	{ return mCamScreenBaseWidth/(float)mCamScreenBaseHeight; }
	//public:

	virtual float getSurfaceVpWidth(){ return (float)G_SCREEN_WIDTH; }
	virtual float getSurfaceVpHeight(){ return (float)G_SCREEN_HEIGHT; }
	float getSurfaceVpRatio()
	{ return getSurfaceVpWidth()/getSurfaceVpHeight(); }

	GDeviceRotation getRotation()
	{
		static const int DEVICE_ROTATION_TO_ANGLE[] = {0,270,180,90};
		switch(mRot)
		{
			case 0:
				return G_DR_0;
			case 270:
				return G_DR_270;
			case 90:
				return G_DR_90;
			case 180:
				return G_DR_180;
			default:
				return G_DR_0;
		}
	}
	void setRotation(GDeviceRotation rot, int restrict = G_NO_RESTRICTION)
	{
		//if portrait
		if((rot == G_DR_90 || rot == G_DR_270))
		{ 
			if((restrict == G_RESTRICT_PORTRAIT && !G_GAME_GLOBAL.isLandscapeOriented()) || (restrict == G_RESTRICT_LANDSCAPE && G_GAME_GLOBAL.isLandscapeOriented()))
				return;
			setParameters(mCamScreenBaseHeight, mCamScreenBaseWidth);
		}
		//ditto
		if((rot == G_DR_0 || rot == G_DR_180))
		{
			if((restrict == G_RESTRICT_LANDSCAPE && !G_GAME_GLOBAL.isLandscapeOriented()) || (restrict == G_RESTRICT_PORTRAIT && G_GAME_GLOBAL.isLandscapeOriented()))
				return;
			setParameters(mCamScreenBaseWidth,mCamScreenBaseHeight);
		}
		mRot = DEVICE_ROTATION_TO_ANGLE[rot];
	}
	virtual void setProjectionMatrix()
	{
		//height is thus the distance from bottom to top of the screen in G_DR_0
		//naturally, we use basevpratio since the camera assumes it is set up in the G_DR_0 regardless of device rotation
		
		//this is for the viewport version
		//float w = mTargetHeight/getBaseCamScreenRatio();
		//float h = mTargetHeight;
		//this is the no viewport version
		float w = mTargetHeight/getSurfaceVpRatio();
		float h = mTargetHeight*G_SCREEN_HEIGHT/mCamScreenBaseHeight;
		float eyeY 	= h / 2.0f;
		float fov = 2*atan(eyeY/mCameraDistance);
		float nearDist 	= mCameraDistance / 100.0f;
		float farDist 	= mCameraDistance * 100.0f;	
		float aspect 			= (float)w/(float)h;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		QuPerspective(fov*180/PI, getBaseCamScreenRatio(), nearDist, farDist);	
	}
	virtual void setModelViewMatrix()
	{
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
		QuLookAt(0, 0, mCameraDistance, 0, 0, 0.0, 0.0, 1.0f, 0.0);
		//QuLookAt(mCameraDistance, 0, 0, 0, 0, 0.0, 0.0, 0.0, 1.0f);
		//finally we rotate
		glRotatef(mRot,0,0,1);
	}
	void setScene()
	{
		setProjectionMatrix();
		setModelViewMatrix();
	}
	virtual void drawBlackBars()
	{
		//TODO
	}

	//this assumes in general that aCrd is at default rotation
	virtual QuScreenCoord convertScreenToCameraCoord(const QuScreenCoord & aCrd)
	{
		QuScreenCoord r;
		r.mScreenBox.h = getDrawingSurfaceHeight();
		r.mScreenBox.w = getDrawingSurfaceWidth();

		//should be the same thing
		//int xoff = (aCrd.w -  mTargetHeight/getScreenVpRatio())/2;
		//int yoff = (aCrd.h -  mTargetHeight)/2;
		int xOff = (G_SCREEN_WIDTH -  getSurfaceVpWidth())/2;
		int yOff = (G_SCREEN_HEIGHT -  getSurfaceVpHeight())/2;

		r.mScreenBox.x = (aCrd.mScreenBox.x - xOff)*r.mScreenBox.w/getSurfaceVpWidth();
		r.mScreenBox.y = (aCrd.mScreenBox.y - yOff)*r.mScreenBox.h/getSurfaceVpHeight();

		return r;

	}
	static void setStaticProjection()
	{
		QuBasicCamera(G_SCREEN_WIDTH).setScene();
	}
};

//will center requested dimensions on the screen, and if too large, it will scale until fit
class QuViewportBasicCamera : public QuBasicCamera
{
	float mSurfaceVpWidth,mSurfaceVpHeight;
public:
	void setViewport()
	{
		float sr = G_SCREEN_HEIGHT / (float)G_SCREEN_WIDTH;
		float cr = getBaseCamScreenHeight()/getBaseCamScreenWidth();
		float x, y, w, h;
		if(G_SCREEN_HEIGHT >= getBaseCamScreenHeight() && G_SCREEN_WIDTH >= getBaseCamScreenWidth())
		{
			w = getBaseCamScreenWidth();
			h = getBaseCamScreenHeight();
			y = (G_SCREEN_HEIGHT - h)/2.0f;
			x = (G_SCREEN_WIDTH - w)/2.0f;
		}
		else if(cr < sr)
		{
			//fit width to edge of screen
			w = G_SCREEN_WIDTH;
			h = G_SCREEN_WIDTH * cr;
			x = 0;
			y = (G_SCREEN_HEIGHT - h)/2.0f;
		}
		else
		{
			h = G_SCREEN_HEIGHT;
			w = G_SCREEN_HEIGHT / cr;
			y = 0;
			x = (G_SCREEN_WIDTH-w)/2.0f;
		}
		mSurfaceVpWidth = w; mSurfaceVpHeight = h;
		glViewport(x,y,w,h);
	}
	QuViewportBasicCamera(int aCamScreenWidth,int aCamScreenHeight = -1, float aTargetHeight = DEFAULT_CAMERA_HEIGHT, float aDistance = DEFAULT_CAMERA_DISTANCE):
	QuBasicCamera(aCamScreenWidth,aCamScreenHeight,aTargetHeight,aDistance)
	{
		setViewport();
	}
	virtual float getSurfaceVpWidth(){ return mSurfaceVpWidth; }
	virtual float getSurfaceVpHeight(){ return mSurfaceVpHeight; }
	virtual void setProjectionMatrix()
	{
		float h = mTargetHeight;
		float w = mTargetHeight/getBaseCamScreenRatio();
		float eyeY 	= h / 2.0f;
		float fov = 2*atan(eyeY/mCameraDistance);
		float nearDist 	= mCameraDistance / 100.0f;
		float farDist 	= mCameraDistance * 100.0f;	
		float aspect 			= (float)w/(float)h;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		QuPerspective(fov*180/PI, getBaseCamScreenRatio(), nearDist, farDist);	
	}
};

//same as above except ortho
class QuOrthoCamera : public QuViewportBasicCamera
{
public:
	QuOrthoCamera(int aCamScreenWidth,int aCamScreenHeight = -1, float aTargetHeight = DEFAULT_CAMERA_HEIGHT) : QuViewportBasicCamera(aCamScreenWidth,aCamScreenHeight,aTargetHeight,4999){}
	void setProjectionMatrix()
	{
		float h = mTargetHeight;
		float w = mTargetHeight/(1/getBaseCamScreenRatio());
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrthof( -w/2.0 ,w/2.0, -h/2.0, h/2.0, 0, 9999 );		
	}
};