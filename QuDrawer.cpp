#include "QuDrawer.h"
#include "QuGlobals.h"

bool QuDrawObject::loadImage(std::string filename, float * aTexData, int aTexStride, int aTexStart)
{
	std::cout << "loading imaeg by filename deprecated" << std::endl;
	G_IMAGE_MANAGER.setFlyImage(filename);
	QuStupidPointer<QuBaseImage> img = G_IMAGE_MANAGER.getFlyImage(filename);
	return loadImage(img,aTexData,aTexStride,aTexStart);
}
bool QuDrawObject::loadImage(QuStupidPointer<QuBaseImage> aImage, float * aTexData, int aTexStride, int aTexStart)
{
	//better if we crash here than anywhere else, don't uncomment until you really need it
	//if(aImage.isNull()) return false;

	mImage = aImage;
	mUseImage = true;
	if(aTexData == NULL)
	{
		//TODO there should be a loadimageandsettexture that auto sets the texture
		//otherwise user should use the load vertices function instead
		setCount(4);
		aTexData = mImage->getUVData();
	}
	mT.initialize(aTexData,GL_TEXTURE_COORD_ARRAY,2,aTexStride,aTexStart);
	return true;
}