#pragma once
#include <map>
#include <math.h>
#include <set>
#include <list>
#include <utility>
#include <functional>
#include "QuUtils.h"



//used for void templates...
struct QuUnit {};

//can be float or int
struct QuColor
{
	float r,g,b,a;
	QuColor(){}
	QuColor(float _r, float _g, float _b, float _a = 1):r(_r),g(_g),b(_b),a(_a){}
};

struct Qu32BitColor
{
	unsigned char r,g,b,a;
	Qu32BitColor():r(0),g(0),b(0),a(0){}
	Qu32BitColor(unsigned char _r, unsigned char _g, unsigned char _b, unsigned char _a):r(_r),g(_g),b(_b),a(_a){}
	QuColor toQuColor(){return QuColor(r/255.,g/255.,b/255.,a/255.);}
};

struct QuTimer
{
	QuTimer(int aStart = 0, int aTarget = 0):mStart(aStart),mCurrent(aStart),mTarget(aTarget){}
	virtual ~QuTimer() {}
	virtual void update(int amnt = 1)
	{
		mCurrent += amnt;
	}
	virtual void reset()
	{
		mCurrent = mStart;
	}

	bool isSet()
	{
		return mTarget != mStart;
	}
	QuTimer & operator++(int unused)
	{
		update();
		return *this;
	}
	QuTimer & operator--(int unused)
	{
		//use clamp if this functionality is needed
		//funny, does actually cause a cute bug in my overlayer class for cube 3 :).
		//if(mCurrent - mStart > 0)
			mCurrent--;
		return *this;
	}
	void setTarget(int aTarget)
	{
		mTarget = aTarget;
	}
	void setTargetAndReset(int aTarget)
	{
		setTarget(aTarget);
		reset();
	}
	void expire()
	{
		mCurrent = mTarget;
	}
	void clamp()
	{
		if(isExpired())
			expire();
		if(getTimeSinceStart() < 0)
			reset();
	}
	//timer expires when mCurrent is >= mTarget
	bool isExpired()
	{
		return mCurrent >= mTarget;
	}
	int getTimeSinceStart()
	{
		return mCurrent-mStart;
	}
	int getTimeUntilEnd()
	{
		return mTarget-mCurrent;
	}
	int getTimeTotal()
	{
		return mTarget-mStart;
	}
	float getLinear()
	{
		if(isExpired())
			return 1;
		return (float)(mCurrent-mStart)/(float)(mTarget-mStart);
	}
	float getLinear(float l, float r)
	{
		return getLinear()*(r-l)+l;
	}
	float getSquareRoot()
	{
		return sqrt(getLinear());
	}
	float getSquare()
	{
		float r = getLinear();
		return r*r;
	}
	float getCubed(float l = 0, float r = 1)
	{
		float v = getLinear();
		return v*v*v*(r-l)+l;
	}
	//0 and 0 and 1, max at 0.5
	float getUpsidedownParabola()
	{
		float x = getLinear();
		return (-(x-0.5)*(x-0.5) + 0.25)*4;
	}
	//1 at 0 and 1, 0 at 0.5
	float getParabola(float l = 0, float r = 1)
	{
		return (1-getUpsidedownParabola())*(r-l)+l;
	}
protected:
	int mStart,mCurrent,mTarget;
};

struct QuTriggerTimer : QuTimer
{
	bool mTriggered;
public:
	QuTriggerTimer(int aStart = 0, int aTarget = 0):QuTimer(aStart,aTarget),mTriggered(false){};
	virtual ~QuTriggerTimer() {}
	virtual void reset()
	{
		mTriggered = false;
		QuTimer::reset();
	}
	bool isTriggered()
	{
		if(!mTriggered && isExpired())
		{
			mTriggered = true;
			return true;
		}
		return false;
	}
};

struct QuMultiTriggerTimer : QuTimer
{
	std::set< int > mEvents;
public:
	QuMultiTriggerTimer(int aStart = 0, int aTarget = 0):QuTimer(aStart,aTarget){};
	virtual ~QuMultiTriggerTimer() {}
	virtual void reset()
	{
		mEvents.clear();
		QuTimer::reset();
	}
	bool isTriggered(int aEvent)
	{
		if(mEvents.find(aEvent) != mEvents.end())
			return false;
		if(aEvent >= QuTimer::mCurrent)
		{
			mEvents.insert(aEvent);
			return true;
		}
		return false;
	}
};

struct QuEventTriggerTimer : private QuTimer
{
public:
	//TODO make event functor class
	QuEventTriggerTimer(int aStart = 0, int aTarget = 0):QuTimer(aStart,aTarget){}
	virtual ~QuEventTriggerTimer() {}
	virtual void update()
	{
		QuTimer::update();
		if(mCurrent == mTarget)
			;//TODO trigger event
	}
};


//TODO delete me
class QuStupidPointerHelper
{
public:
	static std::map<void *, int> mSPMap;
	static int add(void * p)
	{
		std::map<void *, int>::iterator it = mSPMap.find(p);
		if(it == mSPMap.end())
		{
			(mSPMap[p] = 0);
			it = mSPMap.find(p);
		}
		(it->second) += 1;
		return it->second;
	}
	static int sub(void * p)
	{
		return (mSPMap[p] -= 1);
	}
	static void remove(void * p)
	{
		mSPMap.erase(p);
	}
	static int getReferenceCount(void * p)
	{
		return mSPMap[p];
	}
};

/*
template<typename T>
class QuStupidPointer
{
	T * mPt;
	int mN;
public:
	//these get called explicitly
	T * rawPointer() const { return mPt; }

	int size() const { return mN; }

	bool isArray() const { return mN > 1; } 

	T * operator->() const { return mPt; }

	T & getReference() const { return *mPt; }

	template<typename S>
	S & getReference() const { return *((S *)mPt); }

	void setNull() { set(NULL,mN); }

	bool isNull() const { return mPt == NULL; }

	T & operator[](const int & i) const { quAssert(mN == -1 || i < mN); return mPt[i]; }

	//does an implicity upcast, so this is safe
	template<typename S>
	QuStupidPointer<S> safeCast() const { return QuStupidPointer<S>(mPt,mN); }

	int getReferenceCount() const
	{ return QuStupidPointerHelper::getReferenceCount(mPt); }

	//do QuStupidPointer(new T(...)) or QuStupidPointer(new T[N],N)
	QuStupidPointer(T * p = NULL, int n = 1):mPt(NULL)
	{
		set(p,n);
	}
	~QuStupidPointer()
	{
		setNull();
	}
	QuStupidPointer & operator=(const QuStupidPointer & o)
	{
		set(o.mPt,o.mN);
		return *this;
	}
	QuStupidPointer(const QuStupidPointer & o):mPt(NULL)
	{
		set(o.mPt,o.mN);
	}
	bool operator==(const QuStupidPointer & o) const
	{
		return mPt == o.mPt;
	}
	//this is just for using in STL containers
	bool operator<(const QuStupidPointer & o) const
	{
		return mPt < o.mPt;
	}

	QuStupidPointer & operator=(T * o)
	{
		//don't do this
		quAssert(false);
		set(o,1);
		return *this;
	}
	bool operator==(const T * o) const
	{
		//don't do this
		quAssert(false); 
		return mPt == o;
	}
	
private:
	//note, these are pass by value thus we are safe from doing self assignment
	void set(T * p, int n)
	{
		//delete old pointer
		if(!isNull())
		{
			if(0 == QuStupidPointerHelper::sub(mPt))
			{
				QuStupidPointerHelper::remove(mPt);
				if(mN == 1)
					delete mPt;
				else
					delete [] mPt;
			}
		}

		mPt = p;
		mN = n;
		if(mPt != NULL)
			QuStupidPointerHelper::add(mPt);
	}
};
*/


template<typename T>
class QuStupidPointer
{
	T * mPt;
	int mN;
	int * mCounter;
public:
	//these get called explicitly
	T * rawPointer() const { return mPt; }

	int size() const { return mN; }

	bool isArray() const { return mN > 1; } 

	T * operator->() const { return mPt; }

	T & getReference() const { return *mPt; }

	const T & getConstReference() const { return *mPt; }

	template<typename S>
	S & getReference() const { return *((S *)mPt); }

	void setNull() { set(NULL,mN,NULL); }

	bool isNull() const { return mPt == NULL; }

	T & operator[](const int & i) const { quAssert(mN == -1 || i < mN); return mPt[i]; }

	int getReferenceCount() const
	{ return *mCounter; }

	//does an implicity upcast, so this is safe
	template<typename S>
	QuStupidPointer<S> safeCast() const 
	{ 
		return QuStupidPointer<S>(mPt,mN,mCounter); 
	}

	template<typename S>
	QuStupidPointer<T>(S * p, int n = 1, int * aCounter = NULL):mPt(NULL),mCounter(NULL)
	{
		set(p,n,aCounter);
	}

	//do QuStupidPointer(new T(...)) or QuStupidPointer(new T[N],N) (or evene QuStupidPointer<T>(NULL,N))
	QuStupidPointer(T * p = NULL, int n = 1, int * aCounter = NULL):mPt(NULL),mCounter(NULL)
	{
		set(p,n,aCounter);
	}
	~QuStupidPointer()
	{
		setNull();
	}
	QuStupidPointer & operator=(const QuStupidPointer & o)
	{
		set(o.mPt,o.mN,o.mCounter);
		return *this;
	}
	QuStupidPointer(const QuStupidPointer & o):mPt(NULL)
	{
		set(o.mPt,o.mN,o.mCounter);
	}
	bool operator==(const QuStupidPointer & o) const
	{
		return mPt == o.mPt;
	}
	//this is just for using in STL containers
	bool operator<(const QuStupidPointer & o) const
	{
		return mPt < o.mPt;
	}

	QuStupidPointer & operator=(T * o)
	{
		//don't do this
		quAssert(false);
		set(o,1,NULL);
		return *this;
	}
	bool operator==(const T * o) const
	{
		//don't do this
		quAssert(false); 
		return mPt == o;
	}
	
private:
	//note, these are pass by value thus we are safe from doing self assignment
	void set(T * p, int n, int * counter)
	{
		//delete old pointer
		if(!isNull())
		{
			if(0 == --(*mCounter))
			{
				delete mCounter;
				mN == 1 ? delete mPt : delete [] mPt;
			}
		}
		mPt = p;
		mN = n;
		if(mPt != NULL)
			counter != NULL ? mCounter = &(++(*counter)) : mCounter = new int(1);
	}
};



//this is a better version of the below
//point conventions
//entry in signal is of the form pair<F, T> where
//T represents value of data at time F
//can operate with undefined or defined "starting value"
template<typename T, typename F = float>
class QuMapSignalBase
{
protected:
	F mStartTime;
	std::map< F, T > mSignal;
public:
	//-------------
	//ctors
	//-------------
	QuMapSignalBase():mStartTime(0){}
	//-------------
	//validity checks
	//-------------
	bool hasStartingValue() const
	{
		if(!hasValues())
			return false;
		return mStartTime == mSignal.begin()->first;
	}
	bool hasValues() const
	{
		return mSignal.size() > 0;
	}
	bool isConsistent() const
	{
		F prev = mSignal.begin()->first;
		if(mStartTime > prev)
			return false;
		for(typename std::template map<F,T>::iterator it = ++mSignal.begin(); it != mSignal.end(); ++it)
		{
			if(it->first <= prev)
				return false;
			prev = it->first;
		}
		return true;
	}
	
	//-------------
	//accessors
	//-------------
	//note this function only checks if the requested time falls before the first or after the last time in the signal
	virtual bool isTimeInRange(const F & at) const
	{
		if(!hasValues())
			return false;
		if(at < mStartTime) 
			return false;
		return at <= mSignal.rbegin()->first;
	}

	virtual F getSignalTotalTime() const
	{
		return (mSignal.rbegin()->first - mSignal.begin()->first);
	}
	
	//TODO delete this or pass in some sort of print functor or somethnig...
	void printValues()
	{
		std::cout << "printing QuMapSignalBase, size: " << mSignal.size() << " values: \n";
		for(typename std::template map<F,T>::iterator it = mSignal.begin(); it != mSignal.end(); ++it)
		{
			std::cout << "entry time: " << it->first << " values x, y, w, h" 
			<< it->second.mScreenBox.x << " " << it->second.mScreenBox.y 
			<< " " << it->second.mScreenBox.w << " " << it->second.mScreenBox.h << "\n";
		}
		std::cout << std::endl;

	}

	//TODO why can't this be const? 
	//this does no error checking but is guaranteed to return if at least one value exists 
	//if time is specified before the first or after the last time in signal, the first/last value is returned. Otherwise an interpolated value is returned
	//template parameter is provided to get properly interpolated values for integeral signals
	template<typename R>
	T getInterpolated(const F & at)
	{
		//if map is say 2000, 3000 and one calls lb(2000) than it returns iterator to 2000
		//if one calls ub(2000) than it returns iterator to 3000
		typename std::template map<F,T>::iterator ub = mSignal.upper_bound(at);
		
		//stupid case checks
		if(ub == mSignal.begin())
			return mSignal.begin()->second;
		if(ub == mSignal.end())
			return (--ub)->second;
			
		//now we should have lb <= at <= ub
		typename std::template map<F,T>::iterator lb = --(typename std::template map<F,T>::iterator(ub));

		F d = ub->first - lb->first;
		R lambda = (at - lb->first)/R(d);
		return lb->second*lambda + ub->second*(1-lambda);
	}
	
	//TODO why can't this be const?
	//returns GREATER time if there is a tie
	//this does no error checking but is guaranteed to return if at least one value exists
	virtual const T getValueClosest(const F & at)
	{
		typename std::template map<F,T>::iterator ub = mSignal.upper_bound(at);
		if(ub == mSignal.begin()) return mSignal.begin()->second;
		if(ub == mSignal.end()) return (--ub)->second;
		typename std::template map<F,T>::iterator lb = --(typename std::template map<F,T>::iterator(ub));
		return at - lb->first > ub->first - at ? ub->second : lb->second;
	}

	virtual typename std::template map<F,T>::iterator getClosestIterator(const F & at)
	{
		typename std::template map<F,T>::iterator ub = mSignal.upper_bound(at);
		if(ub == mSignal.begin() || ub == mSignal.end() || ub == mSignal.end()--)
			return ub;
		typename std::template map<F,T>::iterator lb = --(typename std::template map<F,T>::iterator(ub));
		return at - lb->first > ub->first - at ? ub : lb;

	}
	
	//-------------
	//modifiers
	//-------------
	virtual void prune(F at) //prunes all things at time at or older
	{
		//or should this be a reverse iterator? here we assume iterator goes through map in an ordered fashion
		std::template list<F> p;
		for(typename std::template map<F,T>::iterator it = mSignal.begin(); it != mSignal.end(); ++it)
		{
			if(it->first <= at)
				p.push_back(it->first);
			else
				break;
		}
		for(typename std::template list<F>::iterator it = p.begin(); it != p.end(); ++it)
			mSignal.erase(*it);
	}
	virtual void shiftAbsolute(const F & at)	//this function stinks but werks
	{
		F toShift = at - mStartTime;
		mStartTime = at;
		std::map<F,T> old(mSignal);
		mSignal.clear();
		for(typename std::template map<F,T>::iterator it = old.begin(); it != old.end(); ++it)
			mSignal[it->first + toShift] = it->second;
	}
	virtual void setStartTime(const F & at)
	{
		mStartTime = at;
	}
	virtual void addAbsolute(T & a, const F & at)
	{
		mSignal[at] = T(a);
	}
	virtual void addRelative(T & a, const F & dt)
	{
		mSignal[mSignal.rbegin()->first + dt] = T(a);
	}
	virtual void reset()
	{
		mSignal.clear();
	}
};

// my old double template version which wont compile under GCC :(
//base clase for a discrete signal, remark, this version is inefficient if the time steps are constant
template<typename T>
class QuTimedSignalBase 
{
protected:
	std::list< std::pair<float,T> > mSignal;
	float mExpire;
	float mTimeTotal;
public:
	QuTimedSignalBase(float aExpire = -1):mExpire(aExpire),mTimeTotal(0)
	{
	}
	~QuTimedSignalBase()
	{
	}
	virtual void add(T a, float at = 1)
	{ 
		mSignal.push_front(std::template pair<float,T>(at,a)); 
		mTimeTotal += at;
		if(mExpire == -1)
			return;
		if(mTimeTotal >= mExpire)
		{
			mTimeTotal -= mSignal.back().first;
			mSignal.pop_back();
		}
	}
	virtual void reset()
	{
		mTimeTotal = 0;
		mSignal = std::template list< std::template pair<float,T> >();
	}
};



//for absolute time signal, one passes the absolute time as at instead of time since last call to add
template<typename T>
class QuAbsoluteTimeSignal : public QuTimedSignalBase<std::pair<float,T> >
{
public:
	QuAbsoluteTimeSignal(float aExpire = -1):QuTimedSignalBase<std::pair<float,T> >(aExpire){}

	//NOTE, the default version of this should never be called but a parameter is given none the less so that the base class version is not called
	virtual void add(T a, float at = 0)
	{
		float prev;
		//just to be safe, this makes sure we don't go backwards in time
		if(QuTimedSignalBase<std::pair<float,T> >::mSignal.size() > 0)
		{		
			quAssert(at >=  prev);
			prev = QuTimedSignalBase<std::pair<float,T> >::mSignal.begin()->second->first + QuTimedSignalBase<std::pair<float,T> >::mTimeTotal;
		}
		else
		{
			quAssert(at >= 0);
			prev = at;
		}
		QuTimedSignalBase<std::pair<float,T> >::add(std::pair<float,T>(a,at), at - prev);
	}

};


template<typename T>
class QuAlgebraicTimedSignal : public QuTimedSignalBase<T>
{
	T mDataTotal;
public:
	QuAlgebraicTimedSignal(float aExpire = -1):QuTimedSignalBase<T>(aExpire),mDataTotal(0){}
	//returns average over the last aCount elts added to mSignal
	//TODO this should weight the last signal based on how long it's been there. 
	//e.g. if we had a 0 value with a really large time size, then we would only weigh the zero by a certain percentage
	//TODO again, you need to do this
	//TODO This is broken
	T weightedAverage(float aTime = -1)
	{
		if(aTime == -1 || aTime > QuTimedSignalBase<T>::mTimeTotal)
		{
			if(QuTimedSignalBase<T>::mTimeTotal > 0)
				return mDataTotal/QuTimedSignalBase<T>::mTimeTotal;
			else
				return T(0);
		}

		T sum = T(0);
		float timeSum = 0;
		for(typename std::template list<std::template pair<float,T> >::iterator it = QuTimedSignalBase<T>::mSignal.begin(); timeSum < aTime && it != QuTimedSignalBase<T>::mSignal.end(); it++)
		{
			timeSum += it->first;
			sum += it->second;
		}
		if(timeSum == 0)
			return T(0);
		return sum / timeSum;
	}

	//note, this version will sum at least aTime into the signal and potentially over aTime depending on the time size of the last signal
	T sum(float aTime = -1)
	{
		if(aTime == -1 || aTime > QuTimedSignalBase<T>::mTimeTotal)
			return mDataTotal;
		T sum = T(0);
		float timeSum = 0;
		for(typename std::list<std::pair<float,T> >::iterator it = QuTimedSignalBase<T>::mSignal.begin(); timeSum < aTime && it != QuTimedSignalBase<T>::mSignal.end(); it++)
		{
			timeSum += it->first;
			sum += it->second;
		}
		return sum;
	}
	virtual void add(T a, float at = 1)
	{ 
		QuTimedSignalBase<T>::mSignal.push_front(std::template pair<float,T>(at,a)); 
		QuTimedSignalBase<T>::mTimeTotal += at;
		mDataTotal += a;
		if(QuTimedSignalBase<T>::mExpire == -1)
			return;
		if(QuTimedSignalBase<T>::mTimeTotal >= QuTimedSignalBase<T>::mExpire)
		{
			std::pair<float,T> dat = QuTimedSignalBase<T>::mSignal.back();
			QuTimedSignalBase<T>::mSignal.pop_back();
			QuTimedSignalBase<T>::mTimeTotal -= dat.first;
			mDataTotal -= dat.second;
		}
	}
	T weightedSum(float aTime = -1)
	{
		if(aTime == -1 || aTime > QuTimedSignalBase<T>::mTimeTotal)
			return mDataTotal;
		T sum = T(0);
		float timeSum = 0;

		for(typename std::list<std::pair<float,T> >::iterator it = QuTimedSignalBase<T>::mSignal.begin(); timeSum < aTime && it != QuTimedSignalBase<T>::mSignal.end(); it++)
		{
			timeSum += it->first;
			sum += it->second*it->first;
		}
		return sum;
	}
	QuStupidPointer<T> getTruncatedList(float intervalSize, float startTime, float endTime)
	{
		int intervals = (int)((endTime-startTime)/intervalSize);
		quAssert(intervals > 0);
		QuStupidPointer<T> r = QuStupidPointer<T>(new float[intervals],intervals);
		
		typename std::template list<std::template pair<float,T> >::iterator it = QuTimedSignalBase<T>::mSignal.begin();
		float timeSum = 0;
		for(int i = 0; i < intervals; i++)
		{ 
			float target = startTime + i * intervalSize;
			while(1)
			{
				if(timeSum > target)
				{
					break;
				}
				if(it++ != QuTimedSignalBase<T>::mSignal.end())
					timeSum += it->first;
				else
				{
					it--;
					break;
				}
			}
			//---x---o  where x is target and o is time sum
			if(it == QuTimedSignalBase<T>::mSignal.begin())
				//we are at the front and there is no next value to interpolate from 
				r[i] = it->second;
			else
			{
				float v1 = it->second;
				it--;
				float v2 = it->second;
				float w = timeSum- it->first;
				it++;

				float lambda = (target-w)/(timeSum-w);
				r[i] = lambda*v2 + (1-lambda)*v1;
			}
		}
		return r;
	}
	virtual void reset()
	{
		mDataTotal = 0;
		QuTimedSignalBase<T>::reset();
	}
	virtual int size()
	{
		return QuTimedSignalBase<T>::mSignal.size();
	}
private:
};



/*
//time value pairs are assumed to be value at end of given time interval. e.g.
//(----------o)(------------o)(-------o)(---o)(---------o) where number '-'s is time and o is value
//to make a starting value, add(val,0) and hopefully no division by zero exception will occur haha...
template<typename T>
class QuAlgebraicTimedSignal
{
	//this is retarded but otherwise GCC wont compile when you nest too many templates
	struct MiniPair
	{
		MiniPair(float a1, T a2):first(a1),second(a2),next(NULL),prev(NULL){}
		MiniPair():next(NULL),prev(NULL){}
		float first;
		T second;
		MiniPair * next;
		MiniPair * prev;
	};
	struct MiniIterator
	{
		MiniPair * mElt;
		MiniIterator(MiniPair * aElt):mElt(aElt){}
		MiniIterator & operator++(int dummy){quAssert(mElt->next != NULL); mElt = mElt->next; return *this;}
		MiniIterator & operator--(int dummy){quAssert(mElt->prev != NULL); mElt = mElt->prev; return *this;}
		MiniPair * operator->(){return mElt;}
		bool operator!=(MiniIterator o){return mElt != o.mElt;}
		bool operator==(MiniIterator o){return mElt == o.mElt;}
	};
	class MiniList
	{
		MiniPair * mBegin;
		MiniPair * mEnd;
		int mSize;
	public:
		MiniIterator begin()
		{
			return MiniIterator(mBegin);
		}
		MiniIterator end()
		{
			return MiniIterator(mEnd);
		}
		int size()
		{
			return mSize;
		}
		void clear()
		{
			MiniPair * k = mBegin;
			while(k != NULL)
			{
				MiniPair * t = k->next;
				delete k;
				k = t;
			}
			initialize();
		}
		void initialize() 
		{
			mSize = 0;
			mEnd = new MiniPair();
			mBegin = mEnd;
		}
		MiniList()
		{
			initialize();
		}
		~MiniList()
		{
			clear();
			delete mBegin;
		}
		void push_front(const MiniPair & elt)
		{
			mBegin->prev = new MiniPair(elt.first,elt.second);
			mBegin->prev->next = mBegin;
			mBegin = mBegin->prev;
			mSize++;
		}
		void pop_back()
		{
			quAssert(mBegin != mEnd);
			MiniPair * p = mEnd->prev;
			//one elt case
			if(p == mBegin)
			{
				delete p;
				mBegin = mEnd;
				mEnd->prev = NULL;
			}
			else
			{
				//extra safety check
				quAssert(p->prev != NULL);
				mEnd->prev = p->prev;
				p->prev->next = mEnd;
				delete p;
			}
			mSize--;
		}
		MiniPair back()
		{
			quAssert(mBegin != mEnd);
			return *(mEnd->prev);
		}
	};
	
	T mDataTotal;
	MiniList mSignal;
	T mExpire;
	T mTimeTotal;
public:
	QuAlgebraicTimedSignal(T aExpire = -1):mExpire(aExpire),mTimeTotal(0),mDataTotal(0){}
	//returns average over the last aCount elts added to mSignal
	//TODO this should weight the last signal based on how long it's been there. 
	//e.g. if we had a 0 value with a really large time size, then we would only weigh the zero by a certain percentage
	//TODO again, you need to do this
	T weightedAverage(float aTime = -1)
	{
		if(aTime == -1 || aTime > mTimeTotal)
			return mDataTotal/mTimeTotal;

		//one value case we are allowed to have zero time so this is to
		if(mSignal.size() == 1)
			return mSignal.begin()->second;

		T sum = T(0);
		float timeSum = 0;
		
		for(MiniIterator it(mSignal.begin()); timeSum < aTime && it != mSignal.end(); it++)
		{
			timeSum += it->first;
			sum += it->second * it->first;
		}

		quAssert(timeSum != 0);
		//if(timeSum == 0) return T(0);

		return sum / timeSum;
	}

	//note, this version will sum at least aTime into the signal and potentially over aTime depending on the time size of the last signal
	T weightedSum(float aTime = -1)
	{
		if(aTime == -1 || aTime > mTimeTotal)
			return mDataTotal;
		T sum = T(0);
		float timeSum = 0;

		for(MiniIterator it(mSignal.begin()); timeSum < aTime && it != mSignal.end(); it++)
		{
			timeSum += it->first;
			sum += it->second*it->first;
		}
		return sum;
	}
	virtual void add(T a, float at = 1)
	{ 
		mSignal.push_front(MiniPair(at,a)); 
		mTimeTotal += at;
		mDataTotal += a;
		if(mExpire == -1)
			return;
		while (mTimeTotal >= mExpire)
		{
			MiniPair dat = mSignal.back();
			mSignal.pop_back();
			mTimeTotal -= dat.first;
			mDataTotal -= dat.second;
		}
	}
	//this returns the derivative based on the neighboring two points if in between a segment or 3 points if on a point
	//TODO I think I wrote this code assuming new times get added at the end rather than the beginning so the derivative should probably be negative what this gives
	//in any case, something might be wrong/fishy
	virtual float shortDeriv(float aTime)
	{
		//no derivatives te take
		if(mSignal.size() < 2) return 0;
		//out of bounds case
		if(aTime > mTimeTotal || aTime < 0) return 0;
		//endpoint case
		if(aTime <= mSignal.begin()->first)
			return (mSignal.begin()->second+(mSignal.begin()++)->second)/(mSignal.begin()++)->first;
		//note there is no division by zero here since the 2 point case will be caught by the above case
		if(aTime == mTimeTotal)
			return (mSignal.end()->second + (mSignal.end()--)->second)/(mSignal.end()--)->first;
		//general case
		//start counting until we get to the time we want
		float timeSum = 0;
		MiniIterator it(mSignal.begin());
		for(; it != mSignal.end(); it++)
		{
			timeSum += it->first;
			if(aTime <= timeSum)
				break;
		}
		//on the point case
		//(--------x) where x donets aTime and it is x
		if(aTime == timeSum)
		{
			float denom = (it++)->first;
			float num = it->second;
			denom += ((it--)--)->first;
			num += it->second;
			return num / denom;
		}
		//general case
		//(------x---o) where x denotes aTime and o denotes it
		float denom = it->first;
		float num = it->second;
		num += (it--)->second;
		return num / denom;
	}
	
	//start time and end time are counting backwards
	QuStupidPointer<T> getTruncatedList(float intervalSize, float startTime, float endTime)
	{
		int intervals = (int)((endTime-startTime)/intervalSize);
		quAssert(intervals > 0);
		QuStupidPointer<T> r = QuStupidPointer<T>(new float[intervals],intervals);

		MiniIterator it = mSignal.begin();
		float timeSum = 0;
		for(int i = 0; i < intervals; i++)
		{ 
			float target = startTime + i * intervalSize;
			while(1)
			{
				if(timeSum > target)
				{
					break;
				}
				if(it++ != mSignal.end())
					timeSum += it->first;
				else
				{
					it--;
					break;
				}
			}
			//---x---o  where x is target and o is time sum
			if(it == mSignal.begin())
				//we are at the front and there is no next value to interpolate from 
				r[i] = it->second;
			else
			{
				float v1 = it->second;
				it--;
				float v2 = it->second;
				float w = timeSum- it->first;
				it++;

				float lambda = (target-w)/(timeSum-w);
				r[i] = lambda*v2 + (1-lambda)*v1;
			}
		}
		return r;
	}
	virtual void reset()
	{
		mDataTotal = 0;
		mTimeTotal = 0;
		mSignal.clear();
	}
	virtual int size()
	{
		return mSignal.size();
	}
private:
};*/