#pragma once
#include <iostream>
#include <fstream>
#include "QuClutils.h"

class QuOutStream
{
protected:
	std::ostream* pStr;
public:
	QuOutStream(std::ostream* pStr_)
		:pStr(pStr_){}

	~QuOutStream()
	{delete pStr;}
		
	std::ostream& GetStream(){return *pStr;}
};


class QuInStream
{
protected:
	std::istream* pStr;
public:
	QuInStream(std::istream* pStr_)
		:pStr(pStr_){}

	~QuInStream()
	{delete pStr;}
		
	std::istream& GetStream(){return *pStr;}

};

class QuFileManagerInterface
{
public:
	//TODO test this
	void quickWrite(std::string aFilename, std::string aMessage)
	{
		getWriteStream(aFilename)->GetStream() << aMessage.c_str();
	}
	virtual QuStupidPointer<QuOutStream> getWriteStream(std::string aFilename, bool aBinary = false)=0;
	virtual QuStupidPointer<QuInStream> getReadStream(std::string aFilename, bool aBinary = false)=0;
};


//TODO test these
class QuStdOutStream : public QuOutStream
{
	std::ofstream * mStream;
public:
	QuStdOutStream(std::string aFileName):QuOutStream(0)
	{mStream = new std::ofstream();
		mStream->open(aFileName.c_str());
		if(!mStream->is_open())
			quAssert("false");
		pStr = mStream;
	}
	~QuStdOutStream()
	{
		mStream->close();
	}
};

//TODO test these
class QuStdInStream : public QuInStream
{
	std::ifstream * mStream;
public:
	QuStdInStream(std::string aFileName):QuInStream(0)
	{
		mStream = new std::ifstream();
		mStream->open(aFileName.c_str());
		if(!mStream->is_open())
			quAssert("false");
		pStr = mStream;
	}
	~QuStdInStream()
	{
		mStream->close();
	}
	std::ifstream & getStdStream(){ return * mStream; }

};

class QuStdFileManager : public QuFileManagerInterface
{
public:
	QuStupidPointer<QuOutStream> getWriteStream(std::string aFilename, bool aBinary = false)
	{
		return QuStupidPointer<QuStdOutStream>(new QuStdOutStream(aFilename)).safeCast<QuOutStream>();
	}
	QuStupidPointer<QuInStream> getReadStream(std::string aFilename, bool aBinary = false)
	{
		QuStupidPointer<QuStdInStream> r = new QuStdInStream(aFilename);
		if(!r->getStdStream().is_open())
			return NULL;
		return r.safeCast<QuInStream>();
	}
};
