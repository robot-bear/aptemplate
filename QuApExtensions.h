#pragma once
#include <string>
#include <map>
#include "QuUtils.h"
#include "QuClutils.h"
#include "s3eSound.h"
#include "s3eFile.h"
#include "IwSound.h"
#include "QuSoundManager.h"

using namespace std;
class ApSoundStruct : public QuSoundStructInterface
{
	int mChannel;
	CIwSoundSpec * mSound;
	int32 mFileSize;
	CIwSoundInst * mSoundInst;
	void loadSound(string filename)
	{
		//NOTE this requires sound to have been loaded by resource manager earlier
		mSound = (CIwSoundSpec*)IwGetResManager()->GetResNamed(filename.c_str(), IW_SOUND_RESTYPE_SPEC);
	}
public:
	static int32 OnEndOfSample(void * _systemData, void* userData)
	{
		return 0;
	}
	ApSoundStruct(std::string filename, bool aLoop = false):QuSoundStructInterface(aLoop)
	{
		mSoundInst = NULL;
		loadSound(filename);
		mChannel = -1;
	}
	~ApSoundStruct()
	{
	}
	void play()
	{
		//try and get a free mChannel
		//TODO should use s3eSoundChannelRegister with S3E_CHANNEL_END_SAMPLE so we know when the sound has finished playing
		mSoundInst = mSound->Play();
	}
	void stop()
	{
		IwGetSoundManager()->StopSoundSpec(mSound);
	}
	float getVolume()
	{
		float t = mVolumeTimer.getLinear();
		return (mMinVolume*(1-t) + mMaxVolume*t);
	}
	virtual void setVolume(float aVolume)
	{
		//TODO 
	}
	bool isPlaying()
	{
		if(mSoundInst == NULL)
			return false;
		//TODO this pointer might be pointing to deleted memory, not sure how the sound amanger thingy works. I think it has acache of however many sound instances are set in the effects.itx setting and they get cached and reused
		if(mSoundInst->IsPlaying())
			return true;
		mSoundInst = NULL;
		return false;
	}
};

class ApSoundManager : public QuSoundManagerInterface
{
	map<string,QuStupidPointer<ApSoundStruct> > mSounds;
public:
	ApSoundManager()
	{
	}
	~ApSoundManager()
	{
		for(map<string,QuStupidPointer<ApSoundStruct> >::iterator i = mSounds.begin(); i != mSounds.end(); i++)
			i->second.setNull();
	}
	bool areSoundsPlaying()
	{
		for(std::map<std::string,QuStupidPointer<ApSoundStruct> >::iterator i = mSounds.begin(); i != mSounds.end(); i++)
			if(i->second->isPlaying())
				return true;
		return false;
	}
	QuStupidPointer<QuSoundStructInterface> loadSound(string filename)
	{
		if (mSounds.find(filename) != mSounds.end())
			return mSounds[filename].safeCast<QuSoundStructInterface>();
		mSounds[filename] = QuStupidPointer<ApSoundStruct>(new ApSoundStruct(filename));
		return mSounds[filename].safeCast<QuSoundStructInterface>();
	}
	QuStupidPointer<QuSoundStructInterface> getSound(std::string filename)
	{
		return loadSound(filename);
		//why bother
		if (mSounds.find(filename) == mSounds.end())
			return QuStupidPointer<QuSoundStructInterface>(NULL);
		return mSounds[filename].safeCast<QuSoundStructInterface>();
	}
	void safeDeleteSound(std::string filename)
	{
		if(mSounds[filename].getReferenceCount() == 1)
			mSounds.erase(filename);
	}
	void deleteSound(std::string filename)
	{
		quAssert(mSounds[filename].getReferenceCount() == 1);
		mSounds.erase(filename);
	}
	void playSound(string filename, bool looping = false)
	{
		if (mSounds.find(filename) != mSounds.end())
		{
			mSounds[filename]->play();
			mSounds[filename]->setLoop(looping);
		}
	}
	void stopSound(string filename)
	{
		if (mSounds.find(filename) != mSounds.end())
			mSounds[filename]->stop();
	}
	void update()
	{
		IwGetSoundManager()->Update();
		for(map<string,QuStupidPointer<ApSoundStruct> >::iterator i = mSounds.begin(); i != mSounds.end(); i++)
			i->second->update();
	}
	void setFade(string filename, bool isFadeIn)
	{
		if (mSounds.find(filename) != mSounds.end())
		{
			if(isFadeIn)
				mSounds[filename]->setFade(20,1);
			else
				mSounds[filename]->setFade(20,0);
		}
	}
	bool isFadingOut(string filename)
	{
		if (mSounds.find(filename) != mSounds.end())
			return mSounds[filename]->isFadingOut();
	}
};

#include "QuFile.h"
#include <sstream>
class ApInStream: public QuInStream
{
	std::string sData;
	s3eFile * mFile;
public:
	ApInStream(std::string sFileName, bool bBinary = false):QuInStream(0) 
	{
		mFile = s3eFileOpen(sFileName.c_str(),"r");
		if(mFile == NULL)
			quAssert(false);
		if(!bBinary)
		{
			const int buffSize = 255;
			char buffer[buffSize+1];
			while(s3eFileReadString(buffer,buffSize,mFile) != NULL)
				sData.append(buffer);
		}
		else
		{
			const int buffSize = 256;
			char buffer[buffSize];
			bool flag = false;
			while(!flag)
			{
				uint32 bytesRead = s3eFileRead(buffer,sizeof(char),buffSize,mFile);
				//we are at the end
				if(bytesRead < buffSize*sizeof(char))
					flag = true;
				sData.append(buffer,bytesRead/sizeof(char));
			}

		}
		//if(s3eFileGetError() != S3E_FILE_ERR_EOF)
		//	throw SimpleException(sFileName + " failed to read :( in ApInStream");
		s3eFileClose(mFile);
		if(!bBinary)
			pStr = new std::istringstream(sData);
		else
			pStr = new std::istringstream(sData, std::ios_base::in | std::ios_base::binary);

		std::cout << sFileName << " read succesfully" << std::endl;
	}
	~ApInStream()
	{
	}
};

class ApOutStream: public QuOutStream
{
	std::string sData;
	std::string sFileName;
	s3eFile * mFile;
	bool mBinary;
	std::ostringstream* pStrStr;
public:
	ApOutStream(std::string sFileName_, bool aBinary = false):QuOutStream(0), sFileName(sFileName_), mBinary(aBinary)
	{
		if(mBinary)
			pStrStr = new std::ostringstream(sData,std::ios_base::out | std::ios_base::binary);
		else
			pStrStr = new std::ostringstream(sData);
		pStr = pStrStr;
		mFile = s3eFileOpen(sFileName.c_str(),"w");
		if(mFile == NULL)
			quAssert(false);

	}
	~ApOutStream()
	{
		std::string sOut = pStrStr->str();
		s3eFileWrite(sOut.c_str(),sizeof(char),sOut.length(),mFile);
		s3eFileClose(mFile);
	}
};


class ApFileManager : public QuFileManagerInterface
{
public:
	QuStupidPointer<QuOutStream> getWriteStream(std::string s, bool aBinary = false)
	{
		return new ApOutStream(s,aBinary);
	}
	QuStupidPointer<QuInStream> getReadStream(std::string s, bool aBinary = false)
	{
	
		if(!s3eFileCheckExists(s.c_str()))
			return NULL;
		return new ApInStream(s,aBinary);
	}
};
