#pragma once
#include "QuImage.h"
#include "QuClutils.h"
#include "QuDrawer.h"
#include <map>
#include <queue>


class QuFont
{
	class QuFontFormat
	{

	};
	class QuFontFormatStrict : QuFontFormat
	{

	};
	std::string mDataFilename;
	QuStupidPointer<QuBaseImage> mImage;
public:
	QuStupidPointer<QuDrawObject> getTextDrawObject(std::string aText, int width = -1)
	{
	}
private:
};

class QuBaseImageManager
{
public:
	virtual ~QuBaseImageManager(){}
	virtual QuStupidPointer<QuBaseImage> setFlyImage(std::string filename) = 0;
	virtual QuStupidPointer<QuBaseImage> getFlyImage(std::string filename) = 0;
	virtual void deleteFlyImage(std::string filename) = 0;
	virtual void update(int lines = 100) = 0;
	virtual void cleanUnusedImages() = 0;
};

class QuPngImageManager : public QuBaseImageManager
{
	std::map<std::string,QuStupidPointer<QuBaseImage> > mImages;
	std::map<std::string,QuStupidPointer<QuBaseImage> > mUnloadedImages;
	std::map<std::string,QuStupidPointer<QuFont> > mFonts;
public:
	QuPngImageManager(){}
	~QuPngImageManager() { destroy(); }


	//TODO this should do error checking and return a NULL image if image loading failed
	QuStupidPointer<QuBaseImage> setFlyImage(std::string filename)
	{
		if(isImageLoaded(filename))
			return mImages[filename];
		mUnloadedImages[filename] = mImages[filename] = QuStupidPointer<QuPngImage>(new QuPngImage()).safeCast<QuBaseImage>();
		QuBaseImage & img = mImages[filename].getReference();
		//TODO get rid of me
		((QuPngImage *)&img)->loadImage(filename);
		((QuPngImage *)&img)->finishLoadingImage();
		return mImages[filename];
	}
	QuStupidPointer<QuBaseImage> getFlyImage(std::string filename)
	{
		QuStupidPointer<QuBaseImage> img = setFlyImage(filename);
		//TODO get rid of me
		((QuPngImage *)&img)->finishLoadingImage();
		return img;
	}
	void deleteFlyImage(std::string filename)
	{
		//help prevent bad memory access bugs
		//must rid of all references before deleting image 
		quAssert(mImages[filename].getReferenceCount() == 1);
		mImages.erase(filename);
	}
	void update(int lines = 100)
	{
		if(mUnloadedImages.size() > 0)
		{
			QuBaseImage & img = mUnloadedImages.begin()->second.getReference();
			//TODO get rid of me
			if(((QuPngImage *)&img)->pumpImage(lines))
				mUnloadedImages.erase(mUnloadedImages.begin());
		}
	}
	void cleanUnusedImages()
	{
		//TODO
		//check for single reference counted images and add to delte list
		//check if this is in the unused image map
		//delete from map.
	}
private:
	bool isImageLoaded(std::string filename)
	{
		return mImages.find(filename) != mImages.end();
	}
	void destroy()
	{
		
	}
};

//TODO delete me
typedef QuPngImageManager QuImageManager;