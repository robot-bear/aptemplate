#pragma once
#include "QuGlobalConstants.h"
#include "QuConstants.h"
#include "QuClutils.h"
#include "QuUtils.h"
#include "QuSoundManager.h"
#include "QuImageManager.h"
#include "QuGameManager.h"
#include "QuCMath.h"
#include "IwGL.h"
#include "QuApExtensions.h"
#include "QuFile.h"

//we may override these with more efficient implementations
#define G_FRAMERATE QuGlobal::getRef().getFramerate()
#define G_SCREEN_WIDTH QuGlobal::getRef().getScreenWidth()
#define G_SCREEN_HEIGHT QuGlobal::getRef().getScreenHeight()
#define G_SCREEN_RATIO QuGlobal::getRef().getScreenRatio()
#define G_CURRENT_FRAME QuGlobal::getRef().getCurrentFrame()
#define G_DEVICE_ROTATION QuGlobal::getRef().getDeviceRotation()
#define G_GAME_GLOBAL QuGlobal::getRef()
#define G_GAME_MANAGER (QuGlobal::getRef().getManager().getReference())
#define G_SOUND_MANAGER QuGlobal::getRef().getSoundManager()
#define G_IMAGE_MANAGER QuGlobal::getRef().getImageManager()
#define G_FILE_MANAGER QuGlobal::getRef().getFileManager()
#define LOG QuGlobal::getRef().dlog


struct QuGlobalSettings
{
	virtual QuStupidPointer<QuBaseImageManager> getImageManager() { return NULL; }
	virtual QuStupidPointer<QuSoundManagerInterface> getSoundManager() { return NULL; }
	virtual QuStupidPointer<QuFileManagerInterface> getFileManager() { return NULL;	}
	//other settings go here
	bool mRefresh;
	int mFramerate;
	QuGlobalSettings():
	mRefresh(true),mFramerate(G_DEFAULT_INIT_FRAMERATE)
	{}
};

template<class SOUND_TYPE = QuUnit, class IMAGE_TYPE = QuUnit, class FILE_TYPE = QuUnit>
struct QuGlobalTypeSettings : public QuGlobalSettings
{
	//NOTE sizeof(QuUnit) is 1
	//NOTE a better way to do this is to use template specializations but I'm too lazy to do that.
	QuStupidPointer<QuBaseImageManager> getImageManager()
	{
		if(sizeof(IMAGE_TYPE) == 1) return NULL;
		else return new IMAGE_TYPE();
	}
	QuStupidPointer<QuSoundManagerInterface> getSoundManager()
	{
		if(sizeof(SOUND_TYPE) == 1) return NULL;
		else return new SOUND_TYPE();
	}
	QuStupidPointer<QuFileManagerInterface> getFileManager()
	{
		if(sizeof(FILE_TYPE) == 1) return NULL;
		else return new FILE_TYPE();
	}
};

class QuGlobal
{
	QuStupidPointer<QuGlobalSettings> mSettings;
	static QuGlobal * mSelf;
	bool isLoaded;
	bool mQuit;

	QuStupidPointer<QuBaseManager> master;
	QuStupidPointer<QuBaseImageManager> mImageManager;
	QuStupidPointer<QuSoundManagerInterface> mSoundManager;
	QuStupidPointer<QuFileManagerInterface> mFileManager;

	uint64 mCurrentMillis,mLastMillis;
	int mScreenWidth,mScreenHeight;
	int mCurrentFrame;
	QuVector3<float> mAccel;
	GDeviceRotation mRot;
public:
	static QuGlobal & getRef()
	{
		//NOTE not thread safe...
		if(mSelf == NULL)
			mSelf = new QuGlobal();
		return *mSelf;
	}
	void setManager(QuStupidPointer<QuBaseManager> aMan);	//DEPRECATED
	void setManagerAndInitialize(QuStupidPointer<QuBaseManager> aMan);

	template<class T>
	void setManagerAndInitializeByTemplate()
	{
		QuStupidPointer<QuBaseManager> man = new T();//QuStupidPointer<T>(new T()).safeCast<QuBaseManager>();
		man->initialize();
		master = man;
	}

	//TODO template me with some initialization parameters and some initialization arguments
	/*void initialize(QuStupidPointer<QuSoundManagerInterface> aSound)
	{
		mImageManager = QuStupidPointer<QuImageManager>(new QuImageManager());
		//mSoundManager = QuStupidPointer<ApSoundManager>(new ApSoundManager()).safeCast<QuSoundManagerInterface>();
		mSoundManager = aSound;
        mFileManager = QuStupidPointer<QuFileManagerInterface>(new ApFileManager());
		isLoaded = true;
	}*/

	void initialize(QuStupidPointer<QuGlobalSettings> aSettings)
	{
		mSettings = aSettings;
		mImageManager = aSettings->getImageManager();
		mSoundManager = aSettings->getSoundManager();
		mFileManager = aSettings->getFileManager();
		mCurrentMillis = mLastMillis = getMillis();
		isLoaded = true;
	}

	static void destroy()
	{
		if(mSelf)
			mSelf->destroyInternal();
		delete mSelf;
		mSelf = NULL;
	}
	void destroyInternal()
	{
		if(isLoaded)
		{
			master.setNull();
			mSoundManager.setNull();
			mImageManager.setNull();
            mFileManager.setNull();
			isLoaded = false;
		}
	}
	void update()
	{
		//time and frame stuff
		mCurrentFrame++;	
		mLastMillis = mCurrentMillis;
		mCurrentMillis = getMillis();
		mSoundManager->update();
		mImageManager->update();
	}

	QuSoundManagerInterface & getSoundManager()
	{
		return mSoundManager.getReference();
	}
	QuBaseImageManager & getImageManager()
	{
		return mImageManager.getReference();
	}
    QuFileManagerInterface & getFileManager()
    {
        return mFileManager.getReference();
    }
	QuStupidPointer<QuBaseManager> getManager()
	{
		return master;
	}
	int getFramerate(){ return mSettings->mFramerate; }
	int getFrameCount(){ return mCurrentFrame; }
	//TODO put in cpp
	uint64 getMillis(){  return s3eTimerGetMs(); }
	uint64 getDeltaMillis(){ return mCurrentMillis-mLastMillis; }

	bool isLandscapeOriented(){ return mScreenWidth >= mScreenHeight; }
	int getScreenWidth(){return mScreenWidth;}
	int getScreenHeight(){return mScreenHeight;}
	float getScreenRatio(){return mScreenWidth/(float)mScreenHeight;}
	float getRotatedScreenRatio(GDeviceRotation rot){ return (rot == G_DR_0 || rot == G_DR_180) ? getScreenRatio() : 1/getScreenRatio(); }
	QuVector2<int> getRotatedScreenDimensions(GDeviceRotation rot)
	{
		if(rot == G_DR_FOLLOW)
			rot = G_DEVICE_ROTATION;
		if(rot == G_DR_0 || rot == G_DR_180)
			return QuVector2<int>(mScreenWidth,mScreenHeight);
		else
			return QuVector2<int>(mScreenHeight,mScreenWidth);
	}
	int getCurrentFrame(){return mCurrentFrame;}
	QuVector3<float> getAccel(GDeviceRotation rot = G_DR_0);

	GDeviceRotation getDeviceRotation();
	void setFramerate(int aFramerate){ mSettings->mFramerate = aFramerate; }
	void setQuit()	{ mQuit = true; }
	bool shouldQuit() { return mQuit; }


	void dlog(std::string message)
	{
		s3eDebugTracePrintf(message.c_str());
	}


	//used my main()
	void setAccel(QuVector3<float> aAccel);
	void setDeviceRotation(GDeviceRotation aRot);
	//this assume G_DR_0 I hope
	void setScreenSize(int aWidth, int aHeight);
	bool requestRefresh()
	{return mSettings->mRefresh;}


private:
	QuGlobal()
		:isLoaded(false),
		mQuit(false),
		mCurrentFrame(0),
		mAccel(0,0,1),
		mRot(G_DR_0)
	{
	}
	~QuGlobal()
	{
		quAssert(!isLoaded);
	}
};

