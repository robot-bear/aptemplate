#pragma once
#include "QuConstants.h"

struct TubeCoord
{
public:
	int x, y, z;
	TubeCoord():x(0),y(0),z(0)
	{}
	TubeCoord(int _x, int _y, int _z)
	{
		x = _x; y = _y; z = _z;
	}
	
	//default copy and operator = constructor OKAY
	bool isZero() { return x == 0 && y == 0 && z == 0; }
	bool operator==(const TubeCoord & o) { return x == o.x && y == o.y && z == o.z; }
	bool operator!=(const TubeCoord & o) { return ! (x == o.x && y == o.y && z == o.z); }

	int cardinal() { return x*y*z; }
};

class TubeManager
{
};

class Tube
{
	bool isInit;
	TubeCoord dim;
	char * mCube;
public:
	Tube(TubeCoord _dim):isInit(false),mCube(NULL)
	{
		initialize(_dim);
	}
	void initialize(TubeCoord _dim)
	{
		dim = _dim;
		mCube = new char[dim.cardinal()];
		isInit = true;
	}
	~Tube()
	{
		if(mCube != NULL)
			delete mCube;
	}
private:
};