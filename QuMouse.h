#pragma once
#include "QuClutils.h"
#include "QuGlobals.h"
#include "QuCMath.h"


//this gives us algebraic operators for ScreenCoord
struct QuAlgebraicScreenCoord : public QuScreenCoord
{
	QuAlgebraicScreenCoord(QuScreenCoord & o)
	{
		mScreenBox = o.mScreenBox;
	}
	QuAlgebraicScreenCoord()
	{}
	QuAlgebraicScreenCoord(int aScreenWidth, int aScreenHeight)
	{
		mScreenBox.w = aScreenWidth;
		mScreenBox.h = aScreenHeight;
	}
	QuAlgebraicScreenCoord operator+(const QuAlgebraicScreenCoord & o) const
	{
		QuScreenCoord r(*this);
		r.mScreenBox.x += o.mScreenBox.x;
		r.mScreenBox.y += o.mScreenBox.y;
		return r;
	}
	QuAlgebraicScreenCoord operator-(const QuAlgebraicScreenCoord & o) const
	{
		QuAlgebraicScreenCoord r(*this);
		r.mScreenBox.x -= o.mScreenBox.x;
		r.mScreenBox.y -= o.mScreenBox.y;
		return r;
	}
	QuAlgebraicScreenCoord operator*(const float & s) const
	{
		QuAlgebraicScreenCoord r(*this);
		r.mScreenBox.x *= s;
		r.mScreenBox.y *= s;
		return r;
	}
	QuAlgebraicScreenCoord operator/(const float & s) const
	{
		QuAlgebraicScreenCoord r(*this);
		r.mScreenBox.x /= s;
		r.mScreenBox.y /= s;
		return r;
	}
};

//this keeeps track of the beginning and ending positions of a touch, good for most gestures
struct QuMultiTouchManager
{
	std::map<int,std::pair<QuScreenCoord,QuScreenCoord> > mTouches;
	//call me to update the state
	void multiTouch(int button, bool state,  QuScreenCoord crd)
	{
		if(state)
			mTouches.insert(std::pair<int,std::pair<QuScreenCoord,QuScreenCoord> >(button,std::pair<QuScreenCoord,QuScreenCoord>(crd,crd)));
		else
			mTouches.erase(button);
	}
	//note, this should NOT be called on non touch devices since mouse motion may happen without the mouse actually being clicked
	void multiMotion(int button,  QuScreenCoord crd)
	{
		//usually motion willo nyl trigger when button is down, but in simulation/mouse mode, this is not thecase
		if(mTouches.find(button) == mTouches.end())
			return;
		mTouches[button].second = crd;
	}
	//returns distance between two fingers
	float getFPinch()
	{
		if(mTouches.size() < 2)
			return 0;
		//TODO this is bad since technically it could get screwed up when we touch a third one but whoc ares..
		std::map<int,std::pair<QuScreenCoord,QuScreenCoord> >::iterator it = mTouches.begin();
		std::pair<QuScreenCoord,QuScreenCoord> & first = it->second;
		std::pair<QuScreenCoord,QuScreenCoord> & second = (++it)->second;
		QuFVector2<float> c1 = first.first.getFChange(second.first);
		QuFVector2<float> c2 = first.second.getFChange(second.second);
		float sign = (c1.magnitude() >= c2.magnitude()) ? 1 : -1;
		return ( c2 - c1 ).magnitude() * sign;
	}
};


//this manager will keep track of history of all positions mouse has visited
struct QuAdvancedMultiTouchManager
{
	class FirstAndLastSignal : public QuMapSignalBase<QuAlgebraicScreenCoord>
	{
	public:
		QuAlgebraicScreenCoord & newest()
		{
			return QuMapSignalBase<QuAlgebraicScreenCoord>::mSignal.rbegin()->second;
		}
		QuAlgebraicScreenCoord & oldest()
		{
			return QuMapSignalBase<QuAlgebraicScreenCoord>::mSignal.begin()->second;
		}
		float startTime()
		{
			return QuMapSignalBase<QuAlgebraicScreenCoord>::mSignal.begin()->first;
		}
		//TODO get rid of this srsly... same as addabsolute
		//needed for GCC cuz it cant deal with const argument parameters for whatever reason
		void stupid(QuAlgebraicScreenCoord a, float at)
		{
			QuMapSignalBase<QuAlgebraicScreenCoord>::mSignal[at] = a;
		}
	};

	//TODO figure out what this hould be
	std::map<int,FirstAndLastSignal > mTouches;
public:
	QuAdvancedMultiTouchManager()
	{
	}
	FirstAndLastSignal & getSignal(int aButton)
	{
		return mTouches[aButton];
	}
	//NOTE these functions do no error checking 
	const QuAlgebraicScreenCoord & newest(int i)
	{
		//quAssert(mTouches.find(i) != mTouches.end());
		return mTouches[i].newest();
	}
	const QuAlgebraicScreenCoord& oldest(int i)
	{
		//quAssert(mTouches.find(i) != mTouches.end());
		return mTouches[i].oldest();
	}
	//returns the amount of time the key has been down for.
	float getTimeDown(int i)
	{
		return G_GAME_GLOBAL.getMillis() - mTouches[i].startTime();
	}
	//this looks back at least ms milliseconds and returns change in mouse in the last ms milliseconds
	QuFVector2<float> getFMouseChange(int i, float ms,GDeviceRotation aRot)
	{
		return (newest(i) - mTouches[i].getInterpolated<float>(G_GAME_GLOBAL.getMillis()-ms)).getFPosition().getRotated(DEVICE_ROTATION_TO_ANGLE[aRot]*ANGLES_TO_RADIANS);
	}
	/* 
	QuVector2<int> getIMouseChange(int i, int ms,GDeviceRotation aRot)
	{
		return (newest(i) - mTouches[i].getInterpolated<float>(G_GAME_GLOBAL.getMillis()-ms)).getIPosition();
	}*/
	QuFVector2<float> getTotalFMouseChange(int i, GDeviceRotation aRot)
	{
		return (newest(i) - oldest(i)).getFPosition(aRot);
	}
	QuVector2<int> getTotalIMouseChange(int i, int ms,GDeviceRotation aRot)
	{
		return (newest(i) - oldest(i)).getIPosition(aRot);
	}
	//favors L/R over U/D
	GDirectionalInput getDirectionalMouseChange(int i, float ms, float distance, GDeviceRotation aRot)
	{
		QuFVector2<float> c = getFMouseChange(i, ms,aRot);
		//std::cout << "position " << c.x << " " << c.y << std::endl;
		if(c.magnitude() <= distance)
			return G_DIRECTION_NEUTRAL;
		else if(c.x >= quAbs(c.y))
			return G_RIGHT;
		else if(c.x <= -quAbs(c.y))
			return G_LEFT;
		else if(c.y > 0)
			return G_UP;
		else
			return G_DOWN;
	}
	void multiTouchDown(int button,  QuScreenCoord crd)
	{
		//TODO should be add absolute
		mTouches[button].stupid(QuAlgebraicScreenCoord(crd),float(G_GAME_GLOBAL.getMillis()));
	}
	void multiTouchUp(int button)
	{
		mTouches.erase(button);
	}
	//note, this should NOT be called on non touch devices since mouse motion may happen without the mouse actually being clicked
	//we add a check so this MUST be called after multitouchup
	void multiMotion(int button,  QuScreenCoord crd)
	{
		//TODO should be addabsolute
		if(mTouches.find(button) == mTouches.end()) return;
		getSignal(button).stupid(QuAlgebraicScreenCoord(crd),(float)(G_GAME_GLOBAL.getMillis()));
	}
};
