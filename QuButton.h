#pragma once

#include "QuCMath.h"
#include "QuCamera.h"
#include "QuInterface.h"

class QuBoxButton
{
public:
	//this assumes coord is at G_DR_0 rotationw
	virtual bool click(QuScreenCoord aCrd, GDeviceRotation aRot) = 0;
	//TODO consider making non rotated versions that do not set up the camera themselves for efficiency
	virtual void drawRotatedImage(QuStupidPointer<QuBaseImage> aImg, GDeviceRotation aRot = G_DR_0){}
	virtual void drawRotatedRect(QuColor aColor, GDeviceRotation aRot = G_DR_0){}
	//returns the box in the aRot coordinates
	virtual QuBox2<int> getBox(GDeviceRotation aRot = G_DR_0) = 0;
};

//absolute screen coordinates in int
class QuAbsScreenIntBoxButton : QuBoxButton
{
protected:
	QuBox2<int> mBox;
	QuOrthoCamera mCamera;
public:
	QuAbsScreenIntBoxButton(QuBox2<int> aBox = QuBox2<int>(0,0,0,0)):mBox(aBox),mCamera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT){}
	bool click(QuScreenCoord aCrd, GDeviceRotation aRot)
	{
		//WARNING still not sure which version is right :p
		aCrd.rotate((GDeviceRotation)DEVICE_ROTATION_TO_INVERSE[aRot]);
		//aCrd.rotate(aRot);
		return mBox.doesContainPoint(aCrd.getIPosition());
	}
	void drawRotatedImage(QuStupidPointer<QuBaseImage> aImg, GDeviceRotation aRot = G_DR_0)
	{
		QuImageDrawObject d;
		d.setImage(aImg);
		d.loadBoxVertices(mBox);
		glPushMatrix();
		mCamera.setRotation(aRot);
		mCamera.setScene();
		glDisable(GL_LIGHTING);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glTranslatef(-mCamera.getRotatedWidth()/2.0f,-mCamera.getRotatedHeight()/2.0f,0);
		d.autoDraw();
		glPopMatrix();
	}
	void drawRotatedRect(QuColor aColor, GDeviceRotation aRot = G_DR_0)
	{
		QuShapeDrawer shape;
		shape.setRotation(aRot);
		glDisable(GL_LIGHTING);
		shape.drawRectangle(mBox,aColor);
	}
	void setBox(QuBox2<int> aBox)
	{
		mBox = aBox;
	}
	QuBox2<int> getBox(GDeviceRotation aRot = G_DR_0)
	{
		//TODO consider making a QuScreenBox class for just this sort of thing
		QuScreenCoord pt(mBox.x,mBox.y,G_SCREEN_WIDTH,G_SCREEN_HEIGHT);
		QuScreenCoord sz(mBox.w,mBox.h,G_SCREEN_WIDTH,G_SCREEN_HEIGHT);
		pt.rotate(aRot);
		sz.rotate(aRot);
		return QuBox2<int>(pt.getIPosition().x,pt.getIPosition().y,sz.getIPosition().x,sz.getIPosition().y);
	}
};

//relative coordinates, assumes relative to G_DR_0 position [0,1]x[0,1]
class QuRelScreenFloatBoxButton : QuBoxButton
{
	QuAbsScreenIntBoxButton mConverted;
public:
	QuRelScreenFloatBoxButton(QuBox2<float> aBox = QuBox2<float>(0,0,0,0))
	{ setBox(aBox);}
	bool click(QuScreenCoord aCrd, GDeviceRotation aRot)
	{ return mConverted.click(aCrd,aRot); }
	void setBox(QuBox2<float> aBox)
	{ mConverted.setBox(QuBox2<int>(aBox.x * G_SCREEN_WIDTH,aBox.y * G_SCREEN_HEIGHT, aBox.w * G_SCREEN_WIDTH, aBox.h * G_SCREEN_HEIGHT));}
	void drawRotatedImage(QuStupidPointer<QuBaseImage> aImg, GDeviceRotation aRot = G_DR_0)
	{ mConverted.drawRotatedImage(aImg,aRot); }
	void drawRotatedRect(QuColor aColor, GDeviceRotation aRot = G_DR_0)
	{ mConverted.drawRotatedRect(aColor,aRot); }
	QuBox2<int> getBox(GDeviceRotation aRot = G_DR_0)
	{ return mConverted.getBox(aRot); }
	QuBox2<float> getFloatBox(GDeviceRotation aRot = G_DR_0)
	{
		QuBox2<int> ib = getBox(aRot);
		return QuBox2<float>(ib.x/(float)G_SCREEN_WIDTH,ib.y/(float)G_SCREEN_HEIGHT,ib.w/(float)G_SCREEN_WIDTH,ib.h/(float)G_SCREEN_HEIGHT);
	}
};

class QuEnlargeableAbsScreenIntBoxButton : public QuAbsScreenIntBoxButton
{
	QuBox2<int> mOrigBox;
public:
	//TODO make this a factory functino instead..... see floatbox
	QuEnlargeableAbsScreenIntBoxButton(const QuBaseImage & img, QuVector2<int> pos = QuVector2<int>(0,0), float scale = 1):QuAbsScreenIntBoxButton(QuBox2<int>(pos.x,pos.y,0,0)),mOrigBox(0,0,0,0)
	{
		mOrigBox = QuBox2<int>(pos.x,pos.y,img.getWidth()*scale,img.getHeight()*scale);
		this->setBox(mOrigBox);
	}
	//TODO write a constructor that takes an image  and scale as its argument and automatically sizes to the image
	QuEnlargeableAbsScreenIntBoxButton(QuBox2<int> aBox = QuBox2<int>(0,0,0,0)):mOrigBox(aBox),QuAbsScreenIntBoxButton(aBox){}
	//TODO check orientation...
	void enlargeOffset(bool enlarge = false, int amnt = 0)
	{
		if(enlarge)
		{
			mBox.x = mOrigBox.x - amnt;
			mBox.y = mOrigBox.y - amnt;
			mBox.w = mOrigBox.w + 2 * amnt;
			mBox.h = mOrigBox.h + 2 * amnt;
		}
		else
		{
			mBox = mOrigBox;
		}
	}
	//TODO enlargePercent
};

class QuEnlargeableRelScreenFloatBoxButton : public QuRelScreenFloatBoxButton
{
	QuBox2<float> mOrigBox;
public:
	//TODO write a constructor that takes an image  and scale as its argument and automatically sizes to the image
	QuEnlargeableRelScreenFloatBoxButton(QuBox2<float> aBox = QuBox2<float>(0,0,0,0)):mOrigBox(aBox),QuRelScreenFloatBoxButton(aBox){}
	//TODO check orientation???
	void enlargeOffset(bool enlarge = false, float amnt = 0)
	{
		QuBox2<float> mBox(0,0,0,0);
		if(enlarge)
		{
			mBox.x = mOrigBox.x - amnt * mOrigBox.w;
			mBox.y = mOrigBox.y - amnt * mOrigBox.h;
			mBox.w = mOrigBox.w * (1 + 2 * amnt);
			mBox.h = mOrigBox.h * (1 + 2 * amnt);
		}
		else
		{
			mBox = mOrigBox;
		}
		setBox(mBox);
	}
	
	//here scale is the width as fraction of screen width
	//this class is totally hack right now...
	static QuEnlargeableRelScreenFloatBoxButton fromImageScreenRelativeScaling(const QuBaseImage & img, GDeviceRotation aRot, QuFVector2<float> pos = QuVector2<float>(0,0), float scale = 1, bool offsetToCenter = true)
	{
		//this code is 100 percent hack....
		if(aRot == G_DR_0 || aRot == G_DR_180)
		{
			float w = scale;
			float h = w * img.getHeightToWidthRatio() * G_GAME_GLOBAL.getScreenRatio();
			if(offsetToCenter)
			{
				pos.x -= w/2.0f;
				pos.y -= h/2.0f;
			}
			return QuEnlargeableRelScreenFloatBoxButton(QuBox2<float>(pos.x,pos.y,w,h));
		}
		else
		{
			//float h = scale;
			//float w = h * img.getWidthToHeightRatio() / G_GAME_GLOBAL.getScreenRatio();
			float w = scale/G_GAME_GLOBAL.getScreenRatio();
			float h = w * img.getHeightToWidthRatio() * G_GAME_GLOBAL.getScreenRatio();
			pos.x /= G_GAME_GLOBAL.getScreenRatio();
			pos.y *= G_GAME_GLOBAL.getScreenRatio();
			if(offsetToCenter)
			{
				pos.x -= w/2.0f;
				pos.y -= h/2.0f;
			}

			return QuEnlargeableRelScreenFloatBoxButton(QuBox2<float>(pos.x,pos.y,w,h));
		}
	}
	//TODO consider making image relative scaling version
};


template<typename T>
class QuButtonList
{
	struct ButtonCallBackGroup
	{
		QuStupidPointer<T> button;
		QuStupidPointer<QuBaseImage> image;
		ButtonCallBackGroup(QuStupidPointer<T> aButton, QuStupidPointer<QuBaseImage> aImage = NULL):button(aButton),image(aImage){}
		//TODO callback functor here
	};
	std::vector<ButtonCallBackGroup> mButtons;
public:
	T & getButton(int index)
	{
		if(index >= mButtons.size() || index < 0)
			quAssert(false);
		return mButtons[index].button.getReference();
	}
	void setImage(int index, QuStupidPointer<QuBaseImage> aImage)
	{
		mButtons[index].image = aImage;
	}
	int click(QuScreenCoord aCrd, GDeviceRotation aRot)
	{
		int counter = 0;
		for(typename std::template vector<ButtonCallBackGroup>::iterator it = mButtons.begin(); it != mButtons.end(); it++ )
		{
			if(it->button->click(aCrd,aRot))
				return counter;
			counter++;
		}
		return -1;
	}
	void addButton(QuStupidPointer<T> aButton, QuStupidPointer<QuBaseImage> image = NULL)
	{
		mButtons.push_back(ButtonCallBackGroup(aButton,image));
	}
	void drawAllButtonsWithImages(GDeviceRotation aRot)
	{
		for(typename std::template vector<ButtonCallBackGroup>::iterator it = mButtons.begin(); it != mButtons.end(); it++ )
			if(it->image.isNull())
				it->button->drawRotatedRect(QuColor(1,0,0,1),aRot);
			else
				it->button->drawRotatedImage(it->image,aRot);
	}
	void drawAllButtons(QuColor aColor, GDeviceRotation aRot)
	{
		for(typename std::template vector<ButtonCallBackGroup>::iterator it = mButtons.begin(); it != mButtons.end(); it++ )
			it->button->drawRotatedRect(aColor,aRot);
	}
	
};


/* garbage, delete mes
class QuRelFloatSmoothSlider
{
	float mStartVal,mEndVal,mCurrentVal;
	QuFVector2<float> mStart,mEnd;
public:
	QuRelFloatSmoothSlider(QuFVector2<float> aStart, QuFVector2<float> aEnd, float aInitVal = 0.5f, float aStartVal = 0, float aEndVal = 1):
	  mCurrentVal(aInitVal),mStartVal(aStartVal),mEndVal(aEndVal),mStart(aStart),mEnd(aEnd)
	{}
	float getCurrentValue(){ return mCurrentVal; }
	bool click(QuScreenCoord aCrd)
	{
		float lambda = (mCurrentVal-mStartVal)/(mEndVal-mStartVal);
		QuFVector2<float> center = mStart * (1-lambda) + mEnd  * lambda;
		//TODO finish
		//QuBox2<float> area(center.x + 
	}


};*/