#include "QuGameManager.h"
#include "QuGlobals.h"
#include "QuClutils.h"

class QuSplashManager : public QuBaseManager
{
	QuImageDrawObject mTitle;
	QuBasicCamera mCamera;
	QuColor mBgColor;
public:
	QuSplashManager(QuColor aBgColor = QuColor(0,0,0,1)):mCamera(G_SCREEN_WIDTH,G_SCREEN_HEIGHT,G_SCREEN_HEIGHT),mBgColor(aBgColor){}
	void initialize()
	{
		mTitle.setImage(G_IMAGE_MANAGER.getFlyImage("splash.png"));
	}
	void update()
	{
		glClearColor(mBgColor.r,mBgColor.g,mBgColor.b,mBgColor.a);
		glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);
		if(!G_GAME_GLOBAL.isLandscapeOriented())
			mCamera.setRotation(G_DR_90);
		mCamera.setRotation(G_DEVICE_ROTATION,G_RESTRICT_LANDSCAPE);
		mCamera.setScene();
		glScalef(min(G_SCREEN_WIDTH,G_SCREEN_HEIGHT),min(G_SCREEN_WIDTH,G_SCREEN_HEIGHT),1);
		glDisable(GL_LIGHTING);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		mTitle.autoDraw();
	}
};