#pragma once
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include "QuImage.h"
#include "QuSoundManager.h"
#include "QuConstants.h"
#include "testApp.h"
#include "QuGlobals.h"
#include "QuGameManager.h"
#include "IwGx.h"
#include "IwGL.h"
#include "IwResManager.h"
#include "IwSound.h"
#include "s3ePointer.h"
#include "s3eDevice.h"
#include "s3eKeyboard.h"
#include "QuSplashManager.h"


//TODO get rid of me
//hack, get rid of me or incorporate me into global
int32 myDevicePause(void* sys, void* user)
{
	s3eDeviceUnYield();
	s3eDeviceBacklightOn();
	std::cout << "paused" << std::endl;
	return 0;
}

//1 1 screen dimension just to help prevent division by zero bug
//TODO get rid of me
QuScreenCoord lastMousePosition(0,0,1,1);

void setGameGlobalRotation(s3eSurfaceBlitDirection dir)
{
	GDeviceRotation rot;
	switch(dir)
	{
	case S3E_SURFACE_BLIT_DIR_ROT90:
		rot =  G_DR_90; break;
	case S3E_SURFACE_BLIT_DIR_ROT180:
		rot =  G_DR_180; break;
	case S3E_SURFACE_BLIT_DIR_ROT270:
		rot =  G_DR_270; break;
	default:
		rot =  G_DR_0; break;
	}
	G_GAME_GLOBAL.setDeviceRotation(rot); //needs to be converted, make APmain.h
}
void resize(void * systemData, void * userData)
{
	 s3eSurfaceOrientation * g = ((s3eSurfaceOrientation *)systemData);
	 if(g->m_OrientationChanged)
	 {
		setGameGlobalRotation(g->m_DeviceBlitDirection);
	 }
	 else
	 {
		//TODO this needs to reset the ogl screen context, but it does not :(
		//G_GAME_GLOBAL.setScreenSize(g->m_Width,g->m_Height);
	 }
}

CIwGLPoint convertToGlCoordinates(int x, int y)
{
	return IwGLTransform(CIwGLPoint(x,y));
}

void keyboardcb(s3eKeyboardEvent * systemData, void* userData)
{
	if(systemData->m_Pressed)
		G_GAME_MANAGER.keyDown(systemData->m_Key);
	else
		G_GAME_MANAGER.keyUp(systemData->m_Key);
}
void singleTouch(s3ePointerEvent* event)
{
	//CIwGLPoint p = convertToGlCoordinates(event->m_x,event->m_y);
	CIwGLPoint p = CIwGLPoint(event->m_x,event->m_y);
	QuVector2<int> dim = G_GAME_GLOBAL.getRotatedScreenDimensions(G_DEVICE_ROTATION);
	QuScreenCoord crd(p.x,p.y,dim.x,dim.y);
	crd.rotate((GDeviceRotation)DEVICE_ROTATION_TO_INVERSE[G_GAME_GLOBAL.getDeviceRotation()]);
	crd.reflect(G_SCREEN_REFLECT_VERTICAL);
	lastMousePosition = crd;
	if(event->m_Pressed)
	{
		G_GAME_MANAGER.singleDown(event->m_Button,crd);
	}
	else
		G_GAME_MANAGER.singleUp(event->m_Button,crd);
}

void singleMotion(s3ePointerMotionEvent* event)
{
	//CIwGLPoint p = convertToGlCoordinates(event->m_x,event->m_y);
	CIwGLPoint p = CIwGLPoint(event->m_x,event->m_y);
	QuVector2<int> dim = G_GAME_GLOBAL.getRotatedScreenDimensions(G_DEVICE_ROTATION);
	QuScreenCoord crd(p.x,p.y,dim.x,dim.y);
	crd.rotate((GDeviceRotation)DEVICE_ROTATION_TO_INVERSE[G_GAME_GLOBAL.getDeviceRotation()]);
	crd.reflect(G_SCREEN_REFLECT_VERTICAL);
	//G_GAME_MANAGER.singleMotion(lastMousePosition,crd);
	G_GAME_MANAGER.singleMotion(crd);
	lastMousePosition = crd;
}

void multiTouch(s3ePointerTouchEvent * event)
{
	//CIwGLPoint p = convertToGlCoordinates(event->m_x,event->m_y);
	CIwGLPoint p = CIwGLPoint(event->m_x,event->m_y);
	QuVector2<int> dim = G_GAME_GLOBAL.getRotatedScreenDimensions(G_DEVICE_ROTATION);
	QuScreenCoord crd(p.x,p.y,dim.x,dim.y);
	crd.rotate((GDeviceRotation)DEVICE_ROTATION_TO_INVERSE[G_GAME_GLOBAL.getDeviceRotation()]);
	crd.reflect(G_SCREEN_REFLECT_VERTICAL);
	G_GAME_MANAGER.multiTouch(event->m_TouchID,event->m_Pressed,crd);
}

//does not support mousechange position type stuff, use QuMouse for that instead
void multiMotion(s3ePointerTouchMotionEvent * event)
{
	//CIwGLPoint p = convertToGlCoordinates(event->m_x,event->m_y);
	CIwGLPoint p = CIwGLPoint(event->m_x,event->m_y);
	QuVector2<int> dim = G_GAME_GLOBAL.getRotatedScreenDimensions(G_DEVICE_ROTATION);
	QuScreenCoord crd(p.x,p.y,dim.x,dim.y);
	crd.rotate((GDeviceRotation)DEVICE_ROTATION_TO_INVERSE[G_GAME_GLOBAL.getDeviceRotation()]);
	crd.reflect(G_SCREEN_REFLECT_VERTICAL);
	G_GAME_MANAGER.multiMotion(event->m_TouchID,crd);
}

bool isQuit()
{
	if(G_GAME_GLOBAL.shouldQuit())
		return true;
    bool rtn = s3eDeviceCheckQuitRequest()
    || (s3eKeyboardGetState(s3eKeyEsc) & S3E_KEY_STATE_PRESSED);
    //|| (s3eKeyboardGetState(s3eKeyAbsBSK) & S3E_KEY_STATE_PRESSED);
    if (rtn)
        std::cout << "hard quit" << std::endl;
    return rtn;
}

void yieldCycle()
{
	static int time = 0;
	time = s3eTimerGetMs()-time;
	if(time > G_FRAMERATE)
		s3eDeviceYield(0);
	else
		s3eDeviceYield(G_FRAMERATE-time);
	time = s3eTimerGetMs();
}

void apUpdateLoop()
{
	//TODO put update accel stuff in here
	float x = s3eAccelerometerGetX()/1000.0f;
	float y = s3eAccelerometerGetY()/1000.0f;
	float z = s3eAccelerometerGetZ()/1000.0f;

	//TODO test me
	switch(s3eSurfaceGetInt(S3E_SURFACE_DEVICE_BLIT_DIRECTION) )
	{
		case S3E_SURFACE_BLIT_DIR_ROT90:
			quSwap(x,y); break;
		case S3E_SURFACE_BLIT_DIR_ROT180:
			x *= -1; y *= -1; break;
		case S3E_SURFACE_BLIT_DIR_ROT270:
			quSwap(x,y); x *= -1; y *= -1; break;
		default:
			break;
	}

	G_GAME_GLOBAL.setAccel(QuVector3<float>(x,y,z));
}

static int GL_ROTATE_TO_SURFACE_BLIT_DIRECTION[4] = 
{
	(int)S3E_SURFACE_BLIT_DIR_NORMAL,
	(int)S3E_SURFACE_BLIT_DIR_ROT90,
	(int)S3E_SURFACE_BLIT_DIR_ROT180,
	(int)S3E_SURFACE_BLIT_DIR_ROT270,
};

void lateInit()
{
	//callbacks
	s3eKeyboardRegister(S3E_KEYBOARD_KEY_EVENT, (s3eCallback)keyboardcb, NULL);
	s3ePointerRegister(S3E_POINTER_BUTTON_EVENT, (s3eCallback)singleTouch, NULL);
    s3ePointerRegister(S3E_POINTER_MOTION_EVENT, (s3eCallback)singleMotion, NULL);

	s3ePointerRegister(S3E_POINTER_TOUCH_EVENT, (s3eCallback)multiTouch, NULL);
	s3ePointerRegister(S3E_POINTER_TOUCH_MOTION_EVENT, (s3eCallback)multiMotion, NULL);
	
	s3eSurfaceRegister(S3E_SURFACE_SCREENSIZE, (s3eCallback)resize, NULL);
}

void mainInit()
{
	//ogl initialize
	//IwGLInit();
	
	IwGxInit();
	glMatrixMode(GL_MODELVIEW);
	glActiveTexture(GL_TEXTURE0);

	//sound initialize, needs to be called after iwgxinit()
	IwSoundInit();
	IwResManagerInit();
	IwGetResManager()->AddHandler(new CIwResHandlerWAV);

	//accelerometer
	s3eAccelerometerStart();


	//TODO add in multimotion in here...

	//set game manager info as needed
	G_GAME_GLOBAL.setScreenSize(int(s3eSurfaceGetInt(S3E_SURFACE_WIDTH)),int(s3eSurfaceGetInt(S3E_SURFACE_HEIGHT)));
	apUpdateLoop();	
	setGameGlobalRotation((s3eSurfaceBlitDirection)GL_ROTATE_TO_SURFACE_BLIT_DIRECTION[IwGLGetInt(IW_GL_ROTATE)]);
	//we want to handle rotation ourselves, but we call this function AFTER setGameGlobalRotation b/c we want to get the starting rotation from iwgl
	IwGLSetInt(IW_GL_VIRTUAL_RESOLUTION,0);

	//TODO get rid of this
	s3eDeviceRegister(S3E_DEVICE_PAUSE,myDevicePause,NULL);
}

void mainTerm()
{
	s3eSurfaceUnRegister(S3E_SURFACE_SCREENSIZE, (s3eCallback)resize);
	s3eKeyboardUnRegister(S3E_KEYBOARD_KEY_EVENT, (s3eCallback)keyboardcb);
	s3ePointerUnRegister(S3E_POINTER_BUTTON_EVENT, (s3eCallback)singleTouch);
    s3ePointerUnRegister(S3E_POINTER_MOTION_EVENT, (s3eCallback)singleMotion);

	s3ePointerUnRegister(S3E_POINTER_TOUCH_EVENT, (s3eCallback)multiTouch);
	s3ePointerUnRegister(S3E_POINTER_TOUCH_MOTION_EVENT, (s3eCallback)multiMotion);

	s3eAccelerometerStop();

	IwResManagerTerminate();
	IwSoundTerminate();
	IwGxTerminate();
	//IwGLTerminate();
}


void runGame()
{
	while (!isQuit())
	{
		s3eKeyboardUpdate();
		s3ePointerUpdate();
		apUpdateLoop();

		//update
		G_GAME_GLOBAL.update();
		G_GAME_MANAGER.update();

		if(G_GAME_GLOBAL.requestRefresh())
			IwGLSwapBuffers();
		yieldCycle();
	}
	QuGlobal::destroy();
}
void drawSplashScreen()
{
	//android hack to draw splash screen
	//TODO initialize another one to draw loading image lol
	G_GAME_GLOBAL.setManager(QuStupidPointer<QuBaseManager>(new QuSplashManager()));
	G_GAME_MANAGER.initialize();
	G_GAME_GLOBAL.update();
	G_GAME_MANAGER.update();
	IwGLSwapBuffers();
}

#ifdef IS_QUTEMPLATE_SUBPROJECT
template<typename T>
int INITIALIZE_GAME(QuStupidPointer<QuGlobalSettings> aSettings)
{
	mainInit();
	//was I suppose to switch order of init and set manager??? probably OKAY
	G_GAME_GLOBAL.initialize(aSettings);
	drawSplashScreen();
	G_GAME_GLOBAL.setManager(QuStupidPointer<QuBaseManager>(new T));
	G_GAME_MANAGER.initialize();
	lateInit();
	runGame();
	mainTerm();
	return 0;
}

//legacy
/*
int APMain(QuBaseManager * manager)
{
	mainInit();
	G_GAME_GLOBAL.setManager(QuStupidPointer<QuBaseManager>(manager));
	G_GAME_GLOBAL.initialize();
	G_GAME_MANAGER.initialize();
	runGame();
	mainTerm();
	return 0;
}*/
#endif