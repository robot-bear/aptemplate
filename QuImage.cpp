#include "QuImage.h"
void QuBaseImage::destroy()
{
	if(isLoaded)
		glDeleteTextures(1,&mId);
	isLoaded = false;
	mId = -1;
}
QuStupidPointer<QuBaseImage> QuBaseImage::reincarnate()
{
	isLoaded = false;
	int id = mId; mId = -1;
	QuStupidPointer<QuBaseImage> r(new QuBaseImage());
	r->setIdAndLoad(id);
	return r;
}
bool QuBaseImage::bind()
{
	if(isLoaded)
	{
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,mId); 
	}
	return isLoaded;
}
void QuBaseImage::unbind()
{ 
	glBindTexture(GL_TEXTURE_2D,NULL); 
	glDisable(GL_TEXTURE_2D);
}
float * QuBaseImage::getUVData()
{
	float w = mWidth/(float)mPotW;
	float h = mHeight/(float)mPotH;
	
	//roatetd 90 version
	//float arr[] = {0,0,0,h,w,0,w,h};
	float arr[] = {0,0,w,0,0,h,w,h};
	float * data = new float[8];
	memcpy(data,arr,sizeof(float)*8);
	return data;
}

void QuBaseImage::setIdAndLoad(int _id)
{ 
	QuBaseImage::destroy();
	isLoaded = true; mId = _id; 
}

void QuDynamicImage::setTexture() 
{ 
	QuBaseImage::destroy();
	setIdAndLoad(QuBaseImage::uploadTexture(GL_RGBA,mPotW,mPotH,mImageData.rawPointer()));
	mIsTextureSet = true; 
}

void QuDynamicImage::initialize(int _w, int _h)
{
	mWidth = _w;
	mHeight = _h;
	mPotW = QuCielPowerOfTwo(mWidth);
	mPotH = QuCielPowerOfTwo(mHeight);
	mImageData = QuStupidPointer<unsigned char>(new unsigned char[mPotW*mPotH*4],mPotW*mPotH*4);
	setTexture();
}

bool QuDynamicImage::safeSetPixel(int _x, int _y, unsigned char _r, unsigned char _g, unsigned char _b, unsigned char _a)
{
	if(_x >= 0 && _y >= 0 && _x < mWidth && _y < mHeight)
		return false;
	setPixel( _x,  _y,  _r,  _g,  _b,  _a);
	return true;
}
void QuDynamicImage::setPixel(int _x, int _y, unsigned char _r, unsigned char _g, unsigned char _b, unsigned char _a)
{
	mIsTextureSet = false;
	int index = _y*mPotW + _x;
	index *= 4;
	mImageData[index + 0] = _r;
	mImageData[index + 1] = _g;
	mImageData[index + 2] = _b;
	mImageData[index + 3] = _a;
}
bool QuDynamicImage::safeGetPixel(int _x, int _y, unsigned char & _r, unsigned char & _g, unsigned char & _b, unsigned char & _a)
{
	if(_x >= 0 && _y >= 0 && _x < mWidth && _y < mHeight)
		return false;
	getPixel( _x,  _y,  _r,  _g,  _b,  _a);
	return true;
}
void QuDynamicImage::getPixel(int _x, int _y, unsigned char & _r, unsigned char & _g, unsigned char & _b, unsigned char & _a)
{
	int index = _y*mPotW + _x;
	index *= 4;
	_r = mImageData[index + 0];
	_g = mImageData[index + 1];
	_b = mImageData[index + 2];
	_a = mImageData[index + 3];

}
void QuPngImage::destroy()
{
	loader.setNull();
	QuBaseImage::destroy();
}
bool QuPngImage::reloadImage()
{
	if(mFilename == "")
		return false;
	destroy();
	if(loadImage(mFilename))
	{
		finishLoadingImage();
		return true;
	}
	return false;
}

bool QuPngImage::pumpImage(int rows)
{
	if(isLoaded)
		return true;
	if(loader.isNull())
		return false;
	if(loader->isPngDoneLoading())
		return true;
	if(loader->update(rows))
		return setTexture();
	return false;
}

bool QuPngImage::finishLoadingImage()
{
	if(isLoaded)
		return true;
	loader->update(loader->getHeight());
	return setTexture();
}

bool QuPngImage::loadImage(std::string filename)
{
	mFilename = filename;
	loader = QuStupidPointer<PngLoader>(new PngLoader());
	loader->setFile(filename);
	mWidth = loader->getWidth();
	mHeight = loader->getHeight();
	mPotW = loader->getPotWidth();
	mPotH = loader->getPotHeight();
	return loader->isPngReadyToLoad();
}

bool QuPngImage::loadEntireImage(std::string filename)
{
	if(loadImage(filename))
		return finishLoadingImage();
	return false;
}

bool QuPngImage::setTexture()
{
	if(loader->isPngDoneLoading())
	{
		/*
		GLuint id = -1;
		glPixelStorei(GL_UNPACK_ALIGNMENT,1);
		glGenTextures(1,&id);
		glBindTexture(GL_TEXTURE_2D,id);
		glTexParameterx(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameterx(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);
		//glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_DECAL);
		glTexImage2D(GL_TEXTURE_2D, 0, loader->getFormat(), loader->getPotWidth(), loader->getPotHeight(), 0, loader->getFormat(), GL_UNSIGNED_BYTE, loader->getImageData());
		*/
		setIdAndLoad(
			QuBaseImage::uploadTexture(
				loader->getFormat(),
				loader->getPotWidth(),
				loader->getPotHeight(),
				loader->getImageData()));
		loader.setNull();
		
		return true;
	}
	return false;
}



void PngLoader::readDataFromFileHandle(png_structp png_ptr, png_bytep outBytes, png_size_t byteCountToRead)
{
	if(png_ptr->io_ptr == NULL)
		return;   // add custom error handling here
	s3eFile * file = (s3eFile*)png_ptr->io_ptr;
	const size_t bytesRead = s3eFileRead((void*)outBytes, sizeof(unsigned char), byteCountToRead, file);
	if((png_size_t)bytesRead != byteCountToRead)
	{ 
		return;   // add custom error handling here
	}
}

void PngLoader::abort()
{
	if(fileHandle != NULL)
	{
		s3eFileClose(fileHandle);
		fileHandle = NULL;
	}
	//std::cout << "ABORT image " << std::endl;
	png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
}

void PngLoader::deleteImageData()
{
	if(imageData != NULL)
	{
		s3eFree( imageData );
		imageData = NULL;
	}
	if(rowPointerData != NULL)
	{
		s3eFree( rowPointerData );
		rowPointerData = NULL;
	}
}

bool PngLoader::setFile(std::string filename)
{
	//load the file into memory
	//TODO implement incremental file reading if needed
	fileHandle = s3eFileOpen(filename.c_str(), "rb");
	//TODO error in loading??
	
	const int sigl = 8;
	unsigned char header[sigl];
	s3eFileRead(header, sigl*sizeof(char), 1, fileHandle);
	//TODO error in reading??
	if(!png_check_sig((unsigned char *)header,sigl))
	{
		std::cout << "loading image " << filename << " failed in PngLoader" << std::endl;
		return false;
	}

	//setup the png for reading
	png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING,NULL,NULL,NULL);
	if(png_ptr == NULL){
		abort();
		return false;
	}
	info_ptr = png_create_info_struct(png_ptr);
	if (info_ptr == NULL){
		abort();
		return false;
	}
	if (setjmp(png_jmpbuf(png_ptr)))
	{
		abort();
		assert(false);
		return false;
	}

	//now set our read function
	png_set_read_fn(png_ptr, fileHandle, readDataFromFileHandle);
	
	//already read this much from headr
	png_set_sig_bytes(png_ptr, sigl);

	//lets just try and read in the whole image for now
	//png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY , NULL);

	//okay we are doing things the hard way
	//interlacing
	/*mNumberPasses = png_set_interlace_handling(png_ptr);
	if(mNumberPasses != 1)
	{
		abort();
		s3eDebugTracePrintf("oh no interlaced png encountered");
		return false;
	}*/		

	//parse the header info
	png_read_info(png_ptr, info_ptr);
	png_uint_32 retval = png_get_IHDR(png_ptr, info_ptr,
	   &mW,
	   &mH,
	   &mBitDepth,
	   &mColorType,
	   NULL, NULL, NULL);
	bytesPerRow = png_get_rowbytes(png_ptr, info_ptr);
	bytesPerPixel = bytesPerRow/mW;

	if(retval != 1)
	{
		abort();
		return false;
	}

	//compute potw/h
	potW = QuCielPowerOfTwo(mW);
	potH = QuCielPowerOfTwo(mH);
	//std::cout << potW << " " << potH << std::endl;
	
	imageData = s3eMalloc(potW*potH*bytesPerPixel*sizeof(unsigned char));
	rowPointerData = (png_bytepp)s3eMalloc(mH*sizeof(png_byte *));
	for(int i = 0; i < mH; i++)
		rowPointerData[i] = (png_byte *)imageData+bytesPerPixel*potW*i;

	//READY TO GO
	bReady = true;
	return true;
}


bool PngLoader::update(int rowsToRead)
{
	if(bDone)
		return true;
	rowsToRead = MIN(mH-linesRead,rowsToRead);
	//TODO fix this
	png_read_rows(png_ptr,rowPointerData + linesRead,NULL,rowsToRead);
	linesRead += rowsToRead;
	if(linesRead == mH)
	{
		bDone = true;
		abort();
		return true;
	}
	return false;
}

GLenum PngLoader::getFormat()
{
	switch(mColorType)
	{
	case PNG_COLOR_TYPE_RGB:
		return GL_RGB;
	case PNG_COLOR_TYPE_RGB_ALPHA:
		return GL_RGBA;
	default:
		return GL_RGB;
	}
}