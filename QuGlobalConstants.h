#pragma once

static const int G_DEFAULT_INIT_FRAMERATE = 30;
enum GDeviceRotation
{
	G_DR_0, G_DR_90, G_DR_180, G_DR_270, G_DR_FOLLOW
};

typedef GDeviceRotation QuRotation;

enum GDeviceRotationRestriction
{
	G_NO_RESTRICTION,
	G_RESTRICT_LANDSCAPE,
	G_RESTRICT_PORTRAIT
};

enum GScreenReflection
{
	G_NO_SCREEN_REFLECTION = 0,
	G_SCREEN_REFLECT_VERTICAL = 1,
	G_SCREEN_REFLECT_HORIZONTAL = 2,
	G_SCREEN_REFLECT_BOTH = 3
};

//maps to inverse rotation
static const int DEVICE_ROTATION_TO_INVERSE[] = {G_DR_0,G_DR_270,G_DR_180,G_DR_90};
//static const int DEVICE_ROTATION_TO_INVERSE[] = {G_DR_0,G_DR_90,G_DR_180,G_DR_270};
//maps to rotation angle
static const int DEVICE_ROTATION_TO_ANGLE[] = {0,270,180,90};
//static const int DEVICE_ROTATION_TO_ANGLE[] = {0,90,180,270};



//then L, LU, U, UR, R, RD, D, DL is 1, 3, 2, 6, 4, 12, 8, 9
enum GDirectionalInput
{
	G_DIRECTION_NEUTRAL = 0,
	G_LEFT = 1,
	G_UP = 2,
	G_RIGHT = 4,
	G_DOWN = 8
};

